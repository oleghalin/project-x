﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using MainResource.Events;
using MainResource.Handler;
using MainResource.Teams;
using static MainResource.Models.DatabaseHandler;
using static MainResource.Utility.ChatColors;
using System;

namespace MainResource.Login {
	public class Registration : Script {
		[RemoteEvent("OnPlayerLoginAttempt")]
		public async void OnPlayerLoginAttempt(Client client, string username, string password) {
			if (username == "") {
				client.SendChatMessage($"{Red}Username cannot be empty!");
				return;
			}

			if (password == "") {
				client.SendChatMessage($"{Red}Password cannot be empty!");
				return;
			}

			var (loginSuccess, playerId) = await LoginPlayer(username, password);

			if (!loginSuccess) {
				client.SendChatMessage($"{Red}Incorrect username or password.");
				return;
			}

            PlayerData.Players.Add(client.Handle.Value, new Player(client));
            var player = client.GetFromList();
            player.ID = playerId;
			client.TriggerEvent("LoginResult", true);
            var newUser = await GetPlayerInfo(client);
            player.UpdateName(client);
            if (newUser || player.PlayerInfo.SelectedTeam == TeamEnum.None)
            {
                client.TriggerEvent("login:Close");
                NewPlayer(client);
                Respawn.Respawned(client);
                player.PlayerInfo.SetPlayerClothes(player);
                await player.SaveAsync();
                return;
            }
            player.SendNotification($"Welcome back to G-Zone, {player.PlayerInfo.PlayerName}", "Enjoy your stay", "success");
            Respawn.Respawned(player.Client);
            player.PlayerInfo.SetPlayerSelectedTeam(player);
            client.TriggerEvent("login:Close");
            player.GiveExp(0);
            NAPI.Notification.SendNotificationToAll($"~y~{player.PlayerInfo.PlayerName}~w~ joined.");
            NAPI.Player.SpawnPlayer(player.Client, player.PlayerInfo.Position);
            player.PlayerInfo.SetPlayerClothes(player);
        }

		[RemoteEvent("OnPlayerRegisterAttempt")]
		public async void OnPlayerRegisterAttempt(Client client, string username, string password) {
			if (username.Length < 3) {
				client.SendChatMessage($"{Red}Your Username must be at least 3 characters long!");
				return;
			}

			if (password.Length < 6) {
				client.SendChatMessage($"{Red}Your Password must be at least 6 characters long!");
				return;
			}

			if (await RegisterPlayer(username, password)) {
				client.SendChatMessage($"You have successfully registered as: {Yellow}{username}{White}.");
				return;
			}

			client.SendChatMessage($"{Red}Username is already in use.");
		}

        internal void NewPlayer(Client client)
        {
            var player = client.GetFromList();
            NAPI.Player.SpawnPlayer(client, new Vector3(-206.6852f, -1015.314f, 30.13815f), 341.0918f);
            NAPI.Player.SetPlayerSkin(client, PedHash.FreemodeMale01);
            player.HexColor = "#196b00";
            player.PlayerInfo.Position = new Vector3(-206.6852f, -1015.314f, 30.13815f);
            player.SendNotification($"Welcome to G-Zone, {player.PlayerInfo.PlayerName}", "Enjoy your stay", "success");
            client.TriggerEvent("Tutorial:Start");
        }
    }
}