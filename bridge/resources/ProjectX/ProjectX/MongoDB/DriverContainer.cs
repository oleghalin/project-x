﻿using MainResource.Models.PlayerData;
using MainResource.Models.Houses;
using System;
using MainResource.Models.Factions;
using MainResource.MongoDB.Models.VehicleData;

namespace MainResource.MongoDB
{
    internal class DriverContainer : Driver
    {
        internal void InitializeCollectionHolders()
        {
            Accounts = new CollectionHolder<PlayerLogin>(this);
            Console.WriteLine($"Loaded {Accounts}");
            Factions = new CollectionHolder<Faction>(this);
            Console.WriteLine($"Loaded {Factions}");
            Houses = new CollectionHolder<House>(this);
            Console.WriteLine($"Loaded {Houses}");
            PlayerInfo = new CollectionHolder<PlayerInfo>(this);
            Console.WriteLine($"Loaded {PlayerInfo}");
            PlayerVehicles = new CollectionHolder<PlayerVehicle>(this);
            Console.WriteLine($"Loaded {PlayerVehicles}");
            TeamVehicles = new CollectionHolder<TeamVehicles>(this);
            Console.WriteLine($"Loaded {TeamVehicles}");
        }

        internal CollectionHolder<PlayerLogin> Accounts { get; set; }
        internal CollectionHolder<Faction> Factions { get; set; }
        internal CollectionHolder<House> Houses { get; set; }
        internal CollectionHolder<PlayerInfo> PlayerInfo { get; set; }
        internal CollectionHolder<PlayerVehicle> PlayerVehicles { get; set; }
        internal CollectionHolder<TeamVehicles> TeamVehicles { get; set; }
    }
}
