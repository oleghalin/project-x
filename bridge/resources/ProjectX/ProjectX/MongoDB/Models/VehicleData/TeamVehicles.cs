﻿using MainResource.Classes;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace MainResource.MongoDB.Models.VehicleData
{
    class TeamVehicles : IModel
    {
        [BsonIgnore]
        public string CollectionName { get; set; } = "teamvehicles";
        [BsonId]
        public Guid ID { get; set; }
        [BsonElement]
        public List<TeamVehicle> TeamVehs { get; set; } = new List<TeamVehicle>();
    }
}
