﻿using BCrypt;
using GTANetworkAPI;
using MainResource.Classes;
using MainResource.Events;
using MainResource.Models.Factions;
using MainResource.Models.Houses;
using MainResource.Models.PlayerData;
using MainResource.MongoDB.Models.VehicleData;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static MainResource.Utility.Config;

namespace MainResource.Models {
    internal class DatabaseHandler : Script
    {
        internal DatabaseHandler()
        {
            SetupDatabase();
        }

        internal static async Task<bool> RegisterPlayer(string username, string password) {
            
                var account = await Database.Accounts.FindSingleAsync(a => a.Username == username);

                if (account != null) return false;

                account = new PlayerLogin()
                {
                    Username = username,
                    Password = BCryptHelper.HashPassword(password, BCryptHelper.GenerateSalt())
                };

                await Database.Accounts.InsertSingleAsync(account);
                return true;
        }

        internal static async Task<(bool loginSuccess, Guid playerId)> LoginPlayer(string username, string password) {
            var account = await Database.Accounts.FindSingleAsync(a => a.Username == username);

            if (account == null || !BCryptHelper.CheckPassword(password, account.Password)) return (false, Guid.Empty);
            return (true, account.ID);
        }
        
        internal static async Task<bool> GetPlayerInfo(Client client)
        {
            var player = client.GetFromList();
            var playerInfo = await Database.PlayerInfo.FindSingleAsync(a => a.ID == player.ID);
            var playerVehicles = await Database.PlayerVehicles.FindManyAsync(a => a.OwnerId == player.ID);
            if (playerInfo == null)
            {
                playerInfo = new PlayerInfo
                {
                    ID = player.ID,
                    PlayerName = client.Name
                };
                player.PlayerInfo = playerInfo;
                await Database.PlayerInfo.InsertSingleAsync(playerInfo);
                return true;
            }
            player.PlayerInfo = playerInfo;
            foreach(var vehicle in playerVehicles)
            {
                player.PlayerInfo.Vehicles.Add(vehicle.ID, vehicle);
            }
            UpdateHUD.UpdateCredits(player.Client, player.PlayerInfo.Credits);
            UpdateHUD.UpdateWanted(player.Client, player.PlayerInfo.Wanted);
            return false;
        }


        internal static async Task<Faction> CreateFaction(string factionName, string factionDescription, string factionTeam, Client client)
        {
            var player = client.GetFromList();
            var faction = await Database.Factions.FindSingleAsync(a => a.Name == factionName);

            if (faction != null)
            {
                client.SendChatMessage("Faction already exists with that name!");
                return null;
            }

            faction = new Faction
            {
                Description = factionDescription,
                Name = factionName,
                Team = factionTeam,
                MemberCount = 1,
                Level = 1,
                Experience = 250,

            };

            await Database.Factions.InsertSingleAsync(faction);
            player.PlayerInfo.FactionRank = Enums.FactionRank.Leader;
            return faction;
        }

        internal static async Task<Faction> GetPlayerFaction(Guid factionId)
        {
                var faction = await Database.Factions.FindSingleAsync(a => a.ID == factionId);

                return faction;
        }

        internal static async void CreateHouse(string street, string location, uint price, Vector3 entry, string interior)
        {
            House house = new House
            {
                OwnerId = Guid.Empty,
                Owner = "None",
                Street = street,
                Location = location,
                Interior = interior,
                Price = price,
                Entry = entry,
                ForSale = true
            };

            await Database.Houses.InsertSingleAsync(house);
        }

        internal static async void SpawnHouses()
        {
            var houses = await Database.Houses.FindManyAsync(a => true);
            if (houses == null) return;
            foreach (House house in houses)
            {
                NAPI.Task.Run(() =>
                {
                    var entryShape = NAPI.ColShape.CreateCylinderColShape(house.Entry, 1, 1);
                    entryShape.SetData("House", house);
                    entryShape.SetData("eDown:UI", "house:Open");
                    NAPI.Marker.CreateMarker(20, house.Entry, new Vector3(), new Vector3(180,180,180), 0.5f, new Color(200, 200, 0, 100));
                    NAPI.Blip.CreateBlip(40, house.Entry, 0.5f, 25, "House", 255, 50, true);
                });
            }
        }

        internal static async Task<string> FindFactionUsers(Guid factionguid)
        {
            var factionMembers = await Database.PlayerInfo.FindManyAsync(a => a.FactionId == factionguid);
            var list = new List<(string, int)>();
            foreach (PlayerInfo playerInfo in factionMembers)
            {
                if (playerInfo.PlayerName == null) return null;
                list.Add((playerInfo.PlayerName, Convert.ToInt32(playerInfo.FactionRank)));
            }
            var json = JsonConvert.SerializeObject(list);
            Console.WriteLine(json);
            return json;
        }

        internal static async Task<House> GetHouse(Guid houseId)
        {
                var house = await Database.Houses.FindSingleAsync(a => a.ID == houseId);
                return house;
        }

        internal static async void BuyHouse(Player player, Guid houseId)
        {
            var house = await Database.Houses.FindSingleAsync(a => a.ID == houseId);
            if (house.OwnerId != Guid.Empty)
            {
                var hOwner = await Database.PlayerInfo.FindSingleAsync(a => a.ID == house.OwnerId);
                hOwner.Credits += house.Price;
            }
            house.Owner = player.Client.Name;
            house.OwnerId = player.ID;
            house.ForSale = false;
            await Database.Houses.ReplaceSingleAsync(a => a.ID == house.ID, house);
        }

        internal static async void SellHouse(Guid houseId, bool sell)
        {
            if (sell)
            {
                var house = await Database.Houses.FindSingleAsync(a => a.ID == houseId);
                house.ForSale = true;
                await Database.Houses.ReplaceSingleAsync(a => a.ID == house.ID, house);
            }
            else if (!sell)
            {
                var house = await Database.Houses.FindSingleAsync(a => a.ID == houseId);
                house.ForSale = false;
                await Database.Houses.ReplaceSingleAsync(a => a.ID == house.ID, house);
            }
        }

        internal static async Task<List<TeamVehicle>> GetTeamVehicles()
        {
            var vehs = await Database.TeamVehicles.FindSingleAsync(a => true);
            if (vehs == null)
            {
                TeamVehicles teamVehicles = new TeamVehicles();
                await Database.TeamVehicles.InsertSingleAsync(teamVehicles);
                return teamVehicles.TeamVehs;
            }
            return vehs.TeamVehs;
        }

        internal static async void AddTeamVehicles(List<TeamVehicle> vehicles)
        {
            var vehs = await Database.TeamVehicles.FindSingleAsync(a => true);
            vehs.TeamVehs = vehicles;
            await Database.TeamVehicles.ReplaceSingleAsync(a => true, vehs);
        }
    }
}