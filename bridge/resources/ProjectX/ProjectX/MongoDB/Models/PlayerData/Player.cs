﻿using GTANetworkAPI;
using MainResource.Classes;
using MainResource.Classes.Missions;
using MainResource.Enums;
using MainResource.Events;
using MainResource.Handler;
using MainResource.MongoDB.Models.VehicleData;
using MainResource.Teams;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static MainResource.Utility.Config;

namespace MainResource.Models.PlayerData
{
    internal class Player
    {
        internal Guid ID { get; set; }
        internal PlayerInfo PlayerInfo { get; set; }
        internal Client Client { get; set; }
        internal VehicleInfo VehicleInfo { get; set; } = new VehicleInfo();
        internal Team Team { get; set; } = null;
        internal string HexColor { get; set; } = "#ffffff";
        internal JobIDs Job { get; set; } = JobIDs.None;
        internal ColShape ColShape { get; set; } = null;
        public List<(WeaponHash, int)> PlayerWeapons { get; set; } = new List<(WeaponHash, int)>();
        public Mission Mission { get; set; } = null;
        public Party Party { get; set; } = null;
        internal Player(Client client)
        {
            Client = client;
        }

        public void SendNotification(string title, string content, string variant)
        {
            Client.TriggerEvent("notification", title, content, variant);
        }

        public void PlayerDeath()
        {
            HexColor = "#ffffff";
            PlayerWeapons.Clear();
            PlayerInfo.Weapons = null;
            PlayerInfo.Spawned = false;
            Client.SetSharedData("Alive", false);
            Respawn.SoundOnDeath(Client);
        }
        public void PlayerRespawn()
        {
            if (Team != null)
            {
                HexColor = Team.HexColor;
            }
            else
            {
                HexColor = "#196b00";
            }
            PlayerInfo.Spawned = true;
            Client.SetSharedData("Alive", true);
        }

        public void AddCredits(uint amount)
        {
            if (amount > 0)
            {
                PlayerInfo.Credits += amount;
                UpdateHUD.UpdateCredits(Client, PlayerInfo.Credits);
            }
        }

        public void TakeCredits(uint amount)
        {
            if (amount < PlayerInfo.Credits)
            {
                PlayerInfo.Credits -= amount;
                UpdateHUD.UpdateCredits(Client, PlayerInfo.Credits);
            }
        }

        public void AddWantedPoints(uint amount)
        {
            if (amount > 0)
            {
                PlayerInfo.WantedPoints += amount;
            }
        }

        public void TakeWantedPoints(uint amount)
        {
            if (amount > 0 && amount <= PlayerInfo.WantedPoints)
            {
                PlayerInfo.WantedPoints -= amount;
            }
        }

        public void AddWanted(uint amount)
        {
            if (amount > 0)
            {
                PlayerInfo.Wanted += amount;
                UpdateHUD.UpdateWanted(Client, PlayerInfo.Wanted);
            }
        }

        public void TakeWanted(uint amount)
        {
            if (amount <= PlayerInfo.Wanted)
            {
                PlayerInfo.Wanted -= amount;
                UpdateHUD.UpdateWanted(Client, PlayerInfo.Wanted);
            }
        }

        public void UpdateName(Client client)
        {
            client.Name = $"{PlayerInfo.PlayerName} [{PlayerInfo.Wanted}]";
        }

        public void GiveExp(uint amount)
        {
            if (amount > 0)
            {
                PlayerInfo.EXP += amount;
                Client.SendNotification($"+~g~{amount}~w~ EXP");
            }

            switch (PlayerInfo.EXP)
            {
                case var n when (PlayerInfo.EXP <= 99):
                    if (PlayerInfo.Level == 0) return;
                    PlayerInfo.Level = 0;
                    break;
                case var n when (PlayerInfo.EXP >= 100 && PlayerInfo.EXP <= 999):
                    if (PlayerInfo.Level != 1)
                    {
                        PlayerInfo.Level = 1;
                        Client.SendNotification($"Level {PlayerInfo.Level} Reached!");
                    }
                    break;
                case var n when (PlayerInfo.EXP >= 1000 && PlayerInfo.EXP <= 1999):
                    if (PlayerInfo.Level != 2)
                    {
                        PlayerInfo.Level = 2;
                        Client.SendNotification($"Level {PlayerInfo.Level} Reached!");
                        Client.SendNotification($"You can now select [~r~CRIMINAL~w~] or [~b~GOVERNMENT~w~]");
                    }
                    break;
                default:
                    break;
            }
        }

        public void GiveTeamExp(uint amount)
        {
            if (amount > 0)
            {
                Team.EXP += amount;
                Client.SendNotification($"+~g~{amount}~w~ Team EXP");
            }

            switch (Team.EXP)
            {
                case var n when (Team.EXP <= 99):
                    if (Team.Level == 0) return;
                    Team.Level = 0;
                    break;
                case var n when (Team.EXP >= 100 && Team.EXP <= 999):
                    if (Team.Level != 1)
                    {
                        Team.Level = 1;
                        Client.SendNotification($"Team Level {Team.Level} Reached!");
                    }
                    break;
                case var n when (Team.EXP >= 1000 && Team.EXP <= 1999):
                    if (Team.Level != 2)
                    {
                        Team.Level = 2;
                        Client.SendNotification($"Team Level {Team.Level} Reached!");
                    }
                    break;
                default:
                    break;
            }
        }


        public void GiveWeapon(WeaponHash weapon, int ammo)
        {
            Client.TriggerEvent("GiveWeapon", weapon, ammo);
        }

        public async Task SaveAsync()
        {
            try
            {
                if (PlayerInfo == null) return;
                if (Team != null)
                {
                    switch (PlayerInfo.SelectedTeam)
                    {
                        case TeamEnum.Civilian:
                            var CivTeam = PlayerInfo.Teams.Find(a => a.TeamEnum == TeamEnum.Civilian);
                            var i1 = PlayerInfo.Teams.IndexOf(CivTeam);
                            if (i1 != -1) PlayerInfo.Teams[i1] = Team;
                            break;
                        case TeamEnum.Criminal:
                            var CrimTeam = PlayerInfo.Teams.Find(a => a.TeamEnum == TeamEnum.Criminal);
                            var i2 = PlayerInfo.Teams.IndexOf(CrimTeam);
                            if (i2 != -1) PlayerInfo.Teams[i2] = Team;
                            break;
                        case TeamEnum.Government:
                            var GovTeam = PlayerInfo.Teams.Find(a => a.TeamEnum == TeamEnum.Government);
                            var i3 = PlayerInfo.Teams.IndexOf(GovTeam);
                            if (i3 != -1) PlayerInfo.Teams[i3] = Team;
                            break;
                        default:
                            break;
                    }
                }
                await Database.PlayerInfo.ReplaceSingleAsync(x => x.ID == PlayerInfo.ID, PlayerInfo);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            
        }
    }

    internal static class PlayerData
    {
        internal static Dictionary<int, Player> Players = new Dictionary<int, Player>();
        internal static Player GetFromList(this Client client)
        {
            return Players[client.Handle.Value];
        }
        internal static bool GetPlayer(this Client client, out Player player)
        {
            return Players.TryGetValue(client.Handle.Value, out player);
        }
        internal static bool IsPlayerOnline(this Player player)
        {
            return Players.ContainsValue(player);
        }
    }
}
