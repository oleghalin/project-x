﻿using MainResource.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace MainResource.Models.PlayerData
{
	internal sealed class PlayerLogin : IModel
    {
        [BsonIgnore]
        public string CollectionName { get; set; } = "accounts";
        [BsonId]
        public Guid ID { get; set; }
        [BsonElement("username")]
        public string Username { get; set; }
        [BsonElement("password")]
        public string Password { get; set; }
        [BsonElement("stafflevel")]
        public int StaffLevel { get; set; }
    }
}