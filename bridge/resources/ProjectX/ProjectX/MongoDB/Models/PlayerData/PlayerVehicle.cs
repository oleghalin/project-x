﻿using MainResource.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace MainResource.Models.PlayerData
{
    internal sealed class PlayerVehicle : IModel
    {
        [BsonIgnore]
        public string CollectionName { get; set; } = "playervehicles";
        [BsonId]
        public Guid ID { get; set; }
        [BsonElement("ownerid")]
        public Guid OwnerId { get; set; }
        [BsonElement("vehiclehash")]
        public int VehicleHash { get; set; }
        [BsonElement("vehicle")]
        public string Vehicle { get; set; }
        [BsonElement("class")]
        public int Class { get; set; }
        [BsonElement("primarycolor")]
        public int PrimaryColor { get; set; }
        [BsonElement("secondarycolor")]
        public int SecondaryColor { get; set; }
        [BsonElement("mods")]
        public List<int> Mods { get; set; }
        [BsonElement("worth")]
        public int Worth { get; set; }
    }
}