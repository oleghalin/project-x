﻿using GTANetworkAPI;
using MainResource.Classes;
using MainResource.Enums;
using MainResource.Models.Factions;
using MainResource.Models.Houses;
using MainResource.MongoDB;
using MainResource.Teams;
using MainResource.Teams.Government;
using MainResource.Utility;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MainResource.Models.PlayerData
{
	internal sealed class PlayerInfo : IModel
    {
        [BsonIgnore]
        public string CollectionName { get; set; } = "playerinfo";
        [BsonId]
        public Guid ID { get; set; }
        [BsonElement("player_name")]
        public string PlayerName { get; set; } = null;
        [BsonElement("spawned")]
        public bool Spawned { get; set; } = false;
        [BsonElement("exp")]
        public ulong EXP { get; set; } = 0;
        [BsonElement("level")]
        public uint Level { get; set; } = 0;
        [BsonElement("credits")]
        public uint Credits { get; set; } = 0;
        [BsonElement("wantedpoints")]
        public ulong WantedPoints { get; set; } = 0;
        [BsonElement("wanted")]
        public uint Wanted { get; set; } = 0;
        [BsonElement("position")]
        public Vector3 Position { get; set; } = new Vector3(0, 0, 0);
        [BsonElement("teams")]
        public List<Team> Teams { get; set; } = new List<Team>();
        [BsonElement("selectedteam")]   
        public TeamEnum SelectedTeam { get; set; } = TeamEnum.None;
        [BsonElement("weapons")]
        public string Weapons { get; set; } = null;
        [BsonElement("faction")]
        public Faction Faction { get; set; } = null;
        [BsonElement("factionid")]
        public Guid FactionId { get; set; } = Guid.Empty;
        [BsonElement("factionrank")]
        public FactionRank FactionRank { get; set; } = FactionRank.None;
        [BsonElement("vehicles")]
        [BsonDictionaryOptions(DictionaryRepresentation.ArrayOfArrays)]
        public Dictionary<Guid, PlayerVehicle> Vehicles { get; set; } = new Dictionary<Guid, PlayerVehicle>();
        [BsonElement("house")]
        public House House { get; set; } = null;
        [BsonElement("clothes")]
        public List<Clothes> Clothes { get; set; } = new List<Clothes>();

        internal async void PlayerJoinTeam(Client client, TeamEnum teamEnum)
        {
            if (!client.GetPlayer(out var player)) return;
            switch (teamEnum)
            {
                case TeamEnum.Civilian:
                    Teams.Add(new Team().SetTeam(client, TeamEnum.Civilian));
                    SetPlayerSelectedTeam(player);
                    await player.SaveAsync();
                    break;
                case TeamEnum.Criminal:
                    Teams.Add(new Team().SetTeam(client, TeamEnum.Criminal));
                    SetPlayerSelectedTeam(player);
                    await player.SaveAsync();
                    break;
                case TeamEnum.Government:
                    Teams.Add(new Team().SetTeam(client, TeamEnum.Government));
                    SetPlayerSelectedTeam(player);
                    await player.SaveAsync();
                    break;
                default:
                    client.SendChatMessage("Error joining team");
                    break;
            }
        }

        internal async void PlayerChangeTeam(Player player, TeamEnum teamEnum)
        {
            if (teamEnum == TeamEnum.Government)
            {
                var team = Teams.Find(a => a.TeamEnum == TeamEnum.Criminal);
                Teams.Remove(team);
                PlayerJoinTeam(player.Client, teamEnum);
            }
            else if (teamEnum == TeamEnum.Criminal)
            {
                var team = Teams.Find(a => a.TeamEnum == TeamEnum.Government);
                Teams.Remove(team);
                PlayerJoinTeam(player.Client, teamEnum);
            }
            await player.SaveAsync();
        }

        internal void SetPlayerSelectedTeam(Player player)
        {
            player.Team = Teams.Find(a => a.TeamEnum == SelectedTeam);
            player.HexColor = player.Team.HexColor;
            player.Client.SetSharedData("BlipColor", player.Team.BlipColor);
            SpawnTeamContent(player, player.Team.TeamEnum);
        }

        internal void SpawnTeamContent(Player player, TeamEnum team)
        {
            switch (team)
            {
                case TeamEnum.Criminal: 
                    player.Client.TriggerEvent("Criminal");
                    break;
                case TeamEnum.Government:
                    player.Client.TriggerEvent("Government");
                    Government.GetGear(player.Client, GetPlayerTeam(SelectedTeam).Specialization);
                    break;
                default:
                    break;
            }
        }

        internal void SetPlayerClothes(Player player)
        {
            foreach(var cloth in Clothes)
            {
                player.Client.SetClothes(cloth.Slot, cloth.Drawable, cloth.Texture);
            }
        }

        internal Dictionary<int, ComponentVariation> GetPlayerClothes(Player player)
        {
            var characterClothes = new Dictionary<int, ComponentVariation>();

            foreach (int slot in Enum.GetValues(typeof(CharacterClothes)))
            {
                characterClothes.Add(slot, new ComponentVariation()
                {
                    Drawable = player.Client.GetClothesDrawable(slot),
                    Texture = player.Client.GetClothesTexture(slot)
                });
            }

            return characterClothes;
        }

        internal void SavePlayerClothes(Player player)
        {
            var clothes = GetPlayerClothes(player);
            var cloths = new List<Clothes>();
            foreach (KeyValuePair<int, ComponentVariation> cloth in clothes)
            {
                player.Client.SendChatMessage($"{cloth.Key} - {cloth.Value.Drawable} - {cloth.Value.Texture}");
                cloths.Add(new Clothes(cloth.Key, cloth.Value.Drawable, cloth.Value.Texture));
                Clothes = cloths;
            }
        }

        internal void AddPlayerVehicle(string vehicleName, Vehicle vehicle, int vehicleHash, Player player, int worth)
        {
            PlayerVehicle playerVehicle = new PlayerVehicle
            {
                ID = Guid.NewGuid(),
                OwnerId = player.ID,
                VehicleHash = vehicleHash,
                Vehicle = vehicleName,
                Class = vehicle.Class,
                PrimaryColor = new Color(255, 255, 255, 255).ToInt32(),
                SecondaryColor = new Color(255, 255, 255, 255).ToInt32(),
                Mods = VehicleMods.Mods(vehicle),
                Worth = worth
            };
            Vehicles.Add(playerVehicle.ID ,playerVehicle);
        }

        internal void SpawnPlayerVehicle(Guid id, Player player, Vector3 pos, float rotation)
        {
            if (!Vehicles.TryGetValue(id, out var vehicle)) return;
            NAPI.Task.Run(() =>
            {
                Vehicle veh = NAPI.Vehicle.CreateVehicle(vehicle.VehicleHash, pos, rotation, 0, 0);
                veh.EngineStatus = false;
                veh.CustomPrimaryColor = new Color(vehicle.PrimaryColor);
                veh.CustomSecondaryColor = new Color(vehicle.SecondaryColor);
                Task.Factory.StartNew(() =>
                {
                    for (int i = 0; i < vehicle.Mods.Count; i++)
                    {
                        if (i == 49)
                        {
                            veh.WheelType = vehicle.Mods[i];
                            veh.SetMod(23, vehicle.Mods[23]);
                            continue;
                        }
                        veh.SetMod(i, vehicle.Mods[i]);
                    }
                    player.VehicleInfo.Vehicle = veh;
                    player.VehicleInfo.VehicleID = id;
                    player.VehicleInfo.SetInventorySize(veh);
                    player.Client.SetIntoVehicle(veh, -1);
                });
            });
        }

        internal void UpdatePlayerVehicle(Guid id, Vehicle veh)
        {
            if (!Vehicles.TryGetValue(id, out var vehicle)) return;
            vehicle.PrimaryColor = veh.CustomPrimaryColor.ToInt32();
            vehicle.SecondaryColor = veh.CustomSecondaryColor.ToInt32();
            vehicle.Mods = VehicleMods.Mods(veh);
            Vehicles[id] = vehicle;
        }

        internal int PlayerVehiclesCount(string clas)
        {
            switch (clas)
            {
                case "car":
                    return Vehicles.Where(a => (a.Value.Class != 14 && a.Value.Class != 15 && a.Value.Class != 16)).Count();
                case "air":
                    return Vehicles.Where(a => (a.Value.Class == 15 && a.Value.Class == 16)).Count();
                case "water":
                    return Vehicles.Where(a => a.Value.Class == 14).Count();
                default:
                    return 0;
            }
        }

        internal void DespawnVehicle(Player player)
        {
            if (player.VehicleInfo.Vehicle != null && player.VehicleInfo.VehicleID != Guid.Empty)
            {
                UpdatePlayerVehicle(player.VehicleInfo.VehicleID, player.VehicleInfo.Vehicle);
            }
        }

        internal void SellVehicle(Player player, Guid id)
        {
            if (!Vehicles.TryGetValue(id, out var vehicle)) return;
            player.AddCredits((uint)vehicle.Worth);
            player.SendNotification("Vehicle", $"Sold Vehicle: {vehicle.Vehicle} for {vehicle.Worth}$", "success");
            Vehicles.Remove(vehicle.ID);
        }

        internal Team GetPlayerTeam(TeamEnum teamEnum)
        {
            return Teams.Find(a => a.TeamEnum == teamEnum);
        }
    }
}