﻿using MainResource.Enums;
using MainResource.MongoDB;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MainResource.Models.Factions
{
    internal class Faction : IModel
    {
        [BsonIgnore]
        public string CollectionName { get; set; } = "factions";
        [BsonId]
        public Guid ID { get; set; }
        [BsonElement("name")]
        internal string Name { get; set; }
        [BsonElement("team")]
        internal string Team { get; set; }
        [BsonElement("description")]
        internal string Description { get; set; }
        [BsonElement("membercount")]
        internal int MemberCount { get; set; }
        [BsonElement("experience")]
        internal int Experience { get; set; }
        [BsonElement("level")]
        internal int Level { get; set; }
        internal Dictionary<int,string> Members = new Dictionary<int,string>();
    }
}
