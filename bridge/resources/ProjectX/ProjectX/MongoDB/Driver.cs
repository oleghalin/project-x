﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace MainResource.MongoDB
{
    public class Driver
    {
        public MongoClient Client { get; set; }

        public IMongoDatabase Database { get; set; }

        public string DatabaseName { get; } = "RageMP";

        public Driver()
        {
            Client = new MongoClient();
            BsonSerializer.RegisterSerializer(typeof(decimal), new DecimalSerializer(BsonType.Double, new RepresentationConverter( true, true)));
        }

        public bool LoadDatabase()
        {
            Database = Client.GetDatabase(DatabaseName);
            return Database != null;
        }

        public IMongoCollection<T> GetCollection<T>() where T : IModel, new()
        {
            return Database.GetCollection<T>(new T().CollectionName);
        }
    }
}
