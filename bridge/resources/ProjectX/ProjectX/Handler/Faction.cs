﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using System;
using static MainResource.Models.DatabaseHandler;
using static MainResource.Utility.ChatColors;

namespace MainResource.Handler
{
    internal class Faction : Script
    {
        [RemoteEvent("OnFactionCreate")]
        internal async void OnFactionCreate(Client client, object[] args)
        {
            var player = client.GetFromList();
            var factionTeam = (string)args[0];
            var factionName = (string)args[1];
            var factionDescription = (string)args[2];

            if (factionTeam == null || factionName == null || factionDescription == null)
            {
                client.SendChatMessage("Please fill all fields");
                return;
            }

            var faction = await CreateFaction(factionName, factionDescription, factionTeam, player.Client);
            if (faction == null)
            {
                return;
            }
            player.PlayerInfo.Faction = faction;
            player.PlayerInfo.FactionRank = Enums.FactionRank.Leader;
            player.PlayerInfo.FactionId = player.PlayerInfo.Faction.ID;
            await player.SaveAsync();

            client.SendChatMessage($"Faction {Yellow}{factionName} created succesfully");
        }

        [Command("myfaction")]
        internal async void MyFaction(Client client)
        {
            if (!client.GetPlayer(out var player)) return;

            if (player.PlayerInfo.Faction == null)
            {
                client.SendChatMessage("You are not in a faction");
                return;
            }

            client.TriggerEvent("myfaction:Open");
            var json = await FindFactionUsers(player.PlayerInfo.FactionId);
            client.TriggerEvent("myfaction:setFactionData", player.PlayerInfo.Faction.Name, player.PlayerInfo.Faction.Level, player.PlayerInfo.Faction.Experience, player.PlayerInfo.Faction.MemberCount, player.PlayerInfo.Faction.Description, (int)player.PlayerInfo.FactionRank, json);
        }

        [Command("factionsettings")]
        internal void FactionSettings(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.PlayerInfo.FactionRank == Enums.FactionRank.Leader)
            {
                client.TriggerEvent("factionSetting:Open");
            }
        }

        [RemoteEvent("FactionKick")]
        internal async void FactionKick(Client client, string user)
        {
            if (!client.GetPlayer(out var player)) return;

            if (player.PlayerInfo.FactionRank == Enums.FactionRank.Leader)
            {
                var kickUser = NAPI.Pools.GetAllPlayers().Find(a => a.Name == user);

                if (kickUser != null)
                {
                    var kicking = kickUser.GetFromList();
                    if (kicking == player) return;
                    if (kicking.PlayerInfo.Faction == player.PlayerInfo.Faction)
                    {
                        kicking.PlayerInfo.Faction = null;
                        kicking.Client.SendChatMessage($"You have been kicked from {player.PlayerInfo.Faction.Name}");
                        player.PlayerInfo.Faction.MemberCount -= 1;
                        await player.SaveAsync();
                        await kicking.SaveAsync();
                    }
                }
            }
        }

        [RemoteEvent("FactionInvite")]
        internal void FactionInvite(Client client, string user)
        {
            if (!client.GetPlayer(out var player)) return;

            if (NAPI.Pools.GetAllPlayers().Find(a => a.Name == user) == null)
            {
                client.SendChatMessage($"Could't find user ~y~{user}");
                return;
            }

            var invitedUser = NAPI.Pools.GetAllPlayers().Find(a => a.Name == user);
            var invitedPlayer = invitedUser.GetFromList();

            if (invitedPlayer.PlayerInfo.Faction != null)
            {
                client.SendChatMessage($"User {user} already has a faction");
                return;
            }

            invitedUser.SendChatMessage($"You have been invited to {player.PlayerInfo.Faction.Name}, type {Green}/accept~w~ to join or {Red}/decline~ to decline.");
            invitedUser.SetData("Invited", player.PlayerInfo.Faction);
        }

        [Command("accept")]
        internal async void Accept(Client client)
        {
            if (!client.GetPlayer(out var player)) return;

            if (client.HasData("Invited"))
            {
                var faction = (Models.Factions.Faction)client.GetData("Invited");
                player.PlayerInfo.Faction = faction;
                faction.MemberCount += 1;
                client.SendChatMessage($"You have joined the faction {Yellow}{player.PlayerInfo.Faction.Name}.");
                client.ResetData("Invited");
                player.PlayerInfo.FactionId = player.PlayerInfo.Faction.ID;
                await player.SaveAsync();
            }
        }

        [Command("decline")]
        internal void Decline(Client client)
        {
            if (!client.GetPlayer(out var player)) return;

            if (client.HasData("Invited"))
            {
                var faction = (Models.Factions.Faction)client.GetData("Invited");
                client.SendChatMessage($"You declined the invite to faction {faction.Name}");
                client.ResetData("Invited");
            }
        }

        [RemoteEvent("LeaveFaction")]
        internal async void LeaveFaction(Client client)
        {
            var player = client.GetFromList();

            if (player.PlayerInfo.Faction == null)
            {
                client.SendChatMessage("You are not in a faction");
                return;
            }
            client.SendChatMessage($"Left {player.PlayerInfo.Faction.Name}.");
            player.PlayerInfo.Faction.MemberCount -= 1;
            player.PlayerInfo.Faction = null;
            player.PlayerInfo.FactionId = Guid.Empty;
            await player.SaveAsync();
            player.Client.TriggerEvent("myfaction:Close");
        }
    }
}