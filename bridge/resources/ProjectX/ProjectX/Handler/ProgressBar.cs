﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;

namespace MainResource.Handler
{
    class ProgressBar : ServerEvents
    {
        [Command("progress")]
        internal void Progress(Client client, int duration)
        {
            if (!client.GetPlayer(out var player)) return;
            CreateProgressBar(player, duration, "Test");
        }
        internal static void CreateProgressBar(Player player, int duration, string callevent)
        {
            player.Client.TriggerEvent("Progress:Start", duration, callevent);
        }

        [RemoteEvent("Test")]
        internal void Test(Client client)
        {
            NAPI.Notification.SendNotificationToAll("Progress finished!");
        }
    }
}
