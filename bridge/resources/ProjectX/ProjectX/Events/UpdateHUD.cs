﻿using System;
using GTANetworkAPI;

namespace MainResource.Events {
	internal class UpdateHUD : Script {
		internal static void UpdateCredits(Client client, uint playerMoney)
        {
			client.TriggerEvent("setHudBalance", Convert.ToSingle(playerMoney));
		}

        internal static void UpdateWanted(Client client, uint wanted)
        {
            client.TriggerEvent("setHudWanted", Convert.ToSingle(wanted));
        }
	}
}