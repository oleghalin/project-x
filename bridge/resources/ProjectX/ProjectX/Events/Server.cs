﻿using GTANetworkAPI;
using MainResource.Classes;
using MainResource.Handler;
using MainResource.Models;
using MainResource.Models.PlayerData;
using System;

namespace MainResource.Events
{
    class Server : ServerEvents
    {
        internal override void OnVehicleEnterAttempt(Vehicle vehicle, sbyte seatID, Player player)
        {
            if (vehicle.HasData("TeamVehicle"))
            {
                var teamCar = (TeamVehicle)vehicle.GetData("TeamVehicle");
                
                if (player.PlayerInfo.SelectedTeam == teamCar.TeamEnum)
                {
                    vehicle.Locked = false;
                }
                else
                {
                    vehicle.Locked = true;
                }
            }
        }

        internal override void OnResourceStart()
        {
            //Housing
            DatabaseHandler.SpawnHouses();
            //Misc.
            NAPI.Server.SetGlobalServerChat(false);
            NAPI.Server.SetAutoRespawnAfterDeath(false);
            NAPI.Server.SetAutoSpawnOnConnect(false);
        }
        internal override void OnPlayerConnect(Client client)
        {
            Console.WriteLine($"{client.Name} connected.");
            Respawn.PlayerJoined(client);
            client.TriggerEvent("login:Open");
            client.SetSharedData("Alive", false);
            client.TriggerEvent("CleanBlips");
            client.TriggerEvent("AddHospitalBlips");
        }

        
        internal override void OnPlayerDeath(Client client, Client killer, uint reason)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Client.IsInVehicle) player.Client.WarpOutOfVehicle();
            player.Client.SetSharedData("Alive", false);
            player.PlayerDeath();
            NAPI.Notification.SendNotificationToAll(!killer.IsNull
                ? $"{killer.GetFromList().PlayerInfo.PlayerName} killed {player.PlayerInfo.PlayerName}."
                : $"{player.PlayerInfo.PlayerName} died.");

            NAPI.Task.Run(() => {
                NAPI.Player.SpawnPlayer(player.Client, player.Client.Position);
                player.Client.TriggerEvent("Invincible", true);
            }, 2000);
        }
    }
}
