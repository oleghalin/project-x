﻿using System;
using System.Collections.Generic;
using System.Linq;
using GTANetworkAPI;
using MainResource.Models.PlayerData;
using MainResource.Teams;
using MainResource.Events;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static MainResource.Utility.ChatColors;
using MainResource.Enums;
using MainResource.Classes;

namespace MainResource.Handler
{
    internal class Remote : Script
    {

        [RemoteEvent("Damaged")]
        public void OnPlayerDamaged(Client client, object[] args)
        {
            Client victim = NAPI.Pools.GetAllPlayers().SingleOrDefault(c => c.Value == (int)args[1]);
            Client attacker = NAPI.Pools.GetAllPlayers().SingleOrDefault(c => c.Value == (int)args[0]);
            var victimPlayer = victim.GetFromList();
            var attackerPlayer = attacker.GetFromList();
            if (attackerPlayer.PlayerInfo.SelectedTeam == TeamEnum.Government && attacker.CurrentWeapon == WeaponHash.Nightstick && victimPlayer.PlayerInfo.Wanted >= 1 && !attacker.IsInVehicle)
            {
                if (!(bool)victim.GetSharedData("Alive"))
                {
                    victim.TriggerEvent("Timer:Stop");
                }
                attackerPlayer.PlayerInfo.Credits += 100;
                attacker.SendChatMessage($"You arrested {Red}{victim.Name}{White}!");
                victim.SendChatMessage($"You have been {Red}jailed{White} by {Blue}{attacker.Name}{White} for 1 minute!");
                victimPlayer.PlayerInfo.Wanted = 0;
                UpdateHUD.UpdateWanted(victim, victimPlayer.PlayerInfo.Wanted);
                victim.Position = new Vector3(1719.566f, 2565.783f, 45.56488f);
                NAPI.Notification.SendNotificationToAll($"~r~{victimPlayer.PlayerInfo.PlayerName}~w~ has been arrested by ~b~{attackerPlayer.PlayerInfo.PlayerName}~w~.");
                victim.RemoveAllWeapons();
                victim.TriggerEvent("Timer:Start", 10 * victimPlayer.PlayerInfo.Wanted, "ReleasePrison", "Released in:");
                return;
            }
            if (attackerPlayer.PlayerInfo.SelectedTeam == TeamEnum.Government)
            {
                return;
            }

            if (attackerPlayer.PlayerInfo.Wanted == 10) return;
            attackerPlayer.PlayerInfo.Wanted += 1;
            UpdateHUD.UpdateWanted(attacker, attackerPlayer.PlayerInfo.Wanted);
        }

        [RemoteEvent("GiveWeapon")]
        internal void GiveWeapon(Client client, int weapon, int ammo)
        {
            client.GiveWeapon((WeaponHash)weapon, ammo);
        }

        [RemoteEvent("RemoveWeapon")]
        internal void RemoveWeapon(Client client, int weapon)
        {
            client.RemoveWeapon((WeaponHash)weapon);
        }

        [RemoteEvent("PlayerRespawn")]
        internal void PlayerRespawn(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            client.StopAnimation();
            client.TriggerEvent("Invincible", false);
            var target = client.Position;
            var closest = Hospital.Hospitals.Min(a => a.Item1.DistanceTo(target));
            var hospital = Hospital.Hospitals.Where(a => a.Item1.DistanceTo(target) == closest).FirstOrDefault();
            client.Position = hospital.Item1;
            client.Rotation = new Vector3(0, 0, hospital.Item2);
            player.PlayerRespawn();
            player.SendNotification("Respawn", "You have been transported to the nearest hospital.", "secondary");
        }

        [RemoteEvent("SetPlayerToGround")]
        internal void TEstings(Client client, float height)
        {
            client.Position = client.Position.Subtract(new Vector3(0, 0, height - 1f));
            client.PlayAnimation("amb@world_human_bum_slumped@male@laying_on_left_side@base", "base", (int)(AnimationFlags.Loop));
            client.TriggerEvent("Timer:Start", 10, "PlayerRespawn", "Respawning in: ");
        }

        [RemoteEvent("ReleasePrison")]
        internal void ReleasePrison(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            client.Position = new Vector3(1847.556, 2585.896, 45.672);
            player.SendNotification("Prison", "You have been released from prison.", "secondary");
        }

        [RemoteEvent("GotPlayerWeapons")]
        internal static void OnGotPlayerWeapons(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            player.PlayerWeapons.Clear();
            var weaponObject = (JObject)args[0];
            var weapons = weaponObject.ToObject<Dictionary<uint, int>>();
            foreach (KeyValuePair<uint, int> weapon in weapons)
            {
                player.PlayerWeapons.Add(((WeaponHash)weapon.Key, weapon.Value));
            }

            string json = JsonConvert.SerializeObject(player.PlayerWeapons, Formatting.Indented);
            player.PlayerInfo.Weapons = json;
        }

        [RemoteEvent("eDown")]
        internal static void OnEDown(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;

            if (client.HasData("eDown:UI"))
            {
                var ui = client.GetData("eDown:UI");
                client.TriggerEvent($"{ui}");
            }
            if (client.HasData("eDown:Action"))
            {
                Enums.Action.GetAction(client, (Actions)client.GetData("eDown:Action"));
            }
        }

        /*[RemoteEvent("lDown")]
        internal static void OnLDown(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (client.IsInVehicle)
            {
                if (client.Vehicle == player.VehicleInfo.Vehicle)
                {
                    if (player.VehicleInfo.Vehicle.Locked)
                    {
                        player.VehicleInfo.Vehicle.Locked = false;
                        client.SendNotification($"Vehicle unlocked.");
                    }
                    else
                    {
                        player.VehicleInfo.Vehicle.Locked = true;
                        client.SendNotification($"Vehicle locked.");
                    }
                }
            }
        }*/

        [RemoteEvent("GlobalMessage")]
        internal static void GlobalMessage(Client client, string message)
        {
            if (!client.GetPlayer(out var player)) return;

            if (message.Contains("<"))
            {
                message.Replace("<", "&lt;");
            }
            if (message.Contains(">"))
            {
                message.Replace(">", "&gt;");
            }
            NAPI.Chat.SendChatMessageToAll($"[{Red}GLOBAL{White}] {player.HexColor}{player.PlayerInfo.PlayerName}{White}: {message}");
        }

        [RemoteEvent("LocalMessage")]
        internal static void LocalMessage(Client client, string message)
        {
            if (!client.GetPlayer(out var player)) return;
            foreach (Client recipient in NAPI.Pools.GetAllPlayers())
            {
                if (recipient.Position.DistanceTo2D(player.Client.Position) < 5f)
                {
                    if (message.Contains("<"))
                    {
                        message.Replace("<", "&lt;");
                    }
                    if (message.Contains(">"))
                    {
                        message.Replace(">", "&gt;");
                    }
                    NAPI.Chat.SendChatMessageToPlayer(recipient, $"{player.HexColor}{player.PlayerInfo.PlayerName}{White}: {message}");
                }
            }
        }

        [RemoteEvent("FactionMessage")]
        internal static void FactionMessage(Client client, string message)
        {
            if (!client.GetPlayer(out var player)) return;
            foreach (Client recipient in NAPI.Pools.GetAllPlayers())
            {
                if (!recipient.GetPlayer(out var recipientPlayer)) return;

                if (recipientPlayer.PlayerInfo.FactionId == player.PlayerInfo.FactionId && recipientPlayer.PlayerInfo.FactionId != Guid.Empty)
                {
                    if (message.Contains("<"))
                    {
                        message.Replace("<", "&lt;");
                    }
                    if (message.Contains(">"))
                    {
                        message.Replace(">", "&gt;");
                    }
                    NAPI.Chat.SendChatMessageToPlayer(recipient, $"[{Orange}FACTION{White}] {player.HexColor}{player.PlayerInfo.PlayerName}{White}: {message}");
                }
            }
        }
    }
}