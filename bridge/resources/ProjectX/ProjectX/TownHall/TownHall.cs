﻿using GTANetworkAPI;
using MainResource.Handler;
using MainResource.Models.PlayerData;
using MainResource.Teams;
using MainResource.Enums;

namespace MainResource.TownHall
{
    class Enterance : ServerEvents
    {
        bool Enter { get; set; } = false;
        internal Enterance()
        {
            ColShape entry = NAPI.ColShape.CreateCylinderColShape(new Vector3(233.1486f, -410.4735f, 48.11198f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(233.1486f, -410.4735f, 48.11198f - 1f), new Vector3(), new Vector3(), 2, new Color(255, 255, 255));
            entry.SetData("TownHall:Entry", new Vector3(276.3401f, -268.1776f, 53.93999f));

            ColShape exit = NAPI.ColShape.CreateCylinderColShape(new Vector3(276.3401f, -268.1776f, 53.93999f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(276.3401f, -268.1776f, 53.93999f - 1f), new Vector3(), new Vector3(), 2, new Color(255, 255, 255));
            exit.SetData("TownHall:Exit", new Vector3(233.1486f, -410.4735f, 48.11198f));
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("TownHall:Entry") && !Enter)
            {
                var entry = (Vector3)colShape.GetData("TownHall:Entry");
                player.Client.Position = entry;
                Enter = true;
                NAPI.Task.Run(() =>
                {
                    Enter = false;
                }, delayTime: 2000);
            }
            if (colShape.HasData("TownHall:Exit") && !Enter)
            {
                var exit = (Vector3)colShape.GetData("TownHall:Exit");
                player.Client.Position = exit;
                Enter = true;
                NAPI.Task.Run(() =>
                {
                    Enter = false;
                }, delayTime: 2000);
            }
        }
    }

    class ProfessionSelection : ServerEvents
    {
        internal ProfessionSelection()
        {
            ColShape colShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(273.0485f, -277.6113f, 53.93998f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(273.0485f, -277.6113f, 53.93998f - 1f), new Vector3(), new Vector3(), 2, new Color(255, 255, 0));
            colShape.SetData("ProfessionSelection", "ProfessionSelection:Open");

            ColShape colShape1 = NAPI.ColShape.CreateCylinderColShape(new Vector3(276.4419f, -281.758f, 53.93996f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(276.4419f, -281.758f, 53.93996f - 0.8f), new Vector3(), new Vector3(), 2, new Color(150, 150, 0));
            NAPI.TextLabel.CreateTextLabel("Type /civilian to join Civilians", new Vector3(276.4419f, -281.758f, 53.93996f), 2, 2, 1, new Color(255, 255, 255));
            colShape1.SetData("Civilian", null);
        }

        [Command("civilian")]
        internal void JoinCivilian(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (client.HasData("Civilian"))
            {
                if (player.PlayerInfo.SelectedTeam == TeamEnum.Civilian)
                {
                    client.SendNotification("You are already part of ~y~[CIVILIAN]");
                    return;
                }
                if (player.PlayerInfo.GetPlayerTeam(TeamEnum.Civilian) != null)
                {
                    player.Team = player.PlayerInfo.GetPlayerTeam(TeamEnum.Civilian);
                    GetPlayerProfession(client, (int)player.Team.Specialization, true);
                    return;
                }
                player.PlayerInfo.PlayerJoinTeam(client, TeamEnum.Civilian);
            }
        }
        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("ProfessionSelection") && player.PlayerInfo.SelectedTeam == TeamEnum.Civilian)
            {
                player.Client.SetData("eDown:UI", colShape.GetData("ProfessionSelection"));
            }

            if (colShape.HasData("Civilian"))
            {
                player.Client.SetData("Civilian", null);
            }
        }

        internal override void OnPlayerExitColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("Civilian") && player.Client.HasData("Civilian"))
            {
                player.Client.ResetData("Civilian");
            }
        }

        [RemoteEvent("PlayerSelectJob")]
		public void OnPlayerSelectJob(Client client, int profession) {
            if (!client.GetPlayer(out var player)) return;
            GetPlayerProfession(client, profession, true);
        }

        internal static async void GetPlayerProfession(Client client, int profession, bool townhall)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Team.TeamEnum == TeamEnum.Civilian)
            {
                player.PlayerInfo.SelectedTeam = TeamEnum.Civilian;
                player.HexColor = player.Team.HexColor;
                Respawn.Respawned(client);
                switch (profession)
                {
                    case (int)TeamSpecialization.Mechanic:
                        if (player.PlayerInfo.Spawned && !townhall)
                        {
                            return;
                        }
                        player.Team.Specialization = TeamSpecialization.Mechanic;
                        client.Position = new Vector3(-346.9381f, -133.501f, 39.00962f);
                        client.Rotation = new Vector3(0, 0, 258.1348f);
                        client.SendNotification($"You have selected profession: ~y~MECHANIC~w~.");
                        if (client.HasData("ColShape")) client.ResetData("ColShape");
                        client.TriggerEvent("ProfessionSelection:Close");
                        await player.SaveAsync();
                        break;
                    case (int)TeamSpecialization.Trucker:
                        if (player.PlayerInfo.Spawned && !townhall)
                        {
                            client.TriggerEvent("trucker:Spawn");
                            return;
                        }
                        client.TriggerEvent("ProfessionSelection:Close");
                        client.TriggerEvent("trucker:Spawn");
                        player.Team.Specialization = TeamSpecialization.Trucker;
                        client.Position = new Vector3(789.0233f, -1770.599f, 29.29468f);
                        client.Rotation = new Vector3(0, 0, 269.4635f);
                        client.SendNotification($"You have selected profession: ~y~TRUCKER~w~.");
                        if (client.HasData("ColShape")) client.ResetData("ColShape");
                        await player.SaveAsync();
                        break;
                    case (int)TeamSpecialization.Medic:
                        if (player.PlayerInfo.Spawned && !townhall)
                        {
                            return;
                        }
                        client.TriggerEvent("ProfessionSelection:Close");
                        player.Team.Specialization = TeamSpecialization.Medic;
                        client.Position = new Vector3(275.446f, -1361.11f, 24.5378f);
                        client.SendNotification($"You have selected profession: ~y~MEDIC~w~.");
                        if (client.HasData("ColShape")) client.ResetData("ColShape");
                        await player.SaveAsync();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
