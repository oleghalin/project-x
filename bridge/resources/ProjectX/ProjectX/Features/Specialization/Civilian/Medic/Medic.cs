﻿using GTANetworkAPI;
using MainResource.Enums;
using MainResource.Models.PlayerData;
using MainResource.Teams;
using System.Linq;

namespace MainResource.Features.Professions.Civilian
{
    public class MedicUtility : Script
    {
        bool Enter { get; set; } = false;

        private readonly Vector3 _medicHQ =
           new Vector3(247.264f, -1371.894f, 24.53779f);


        private readonly Vector3 exit_medicHQ =
        new Vector3(324.0096f, -554.3272f, 28.26531f);

        private readonly Vector3 medicHQ =
        new Vector3(275.446f, -1361.11f, 24.5378f);


        private readonly Vector3 enter_medicHQ =
            new Vector3(356.2654f, -597.0281f, 28.77846f);

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStartS()
        {
            ColShape entry = NAPI.ColShape.CreateCylinderColShape(new Vector3(356.2654f, -597.0281f, 28.77846f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(233.1486f, -410.4735f, 48.11198f - 1f), new Vector3(), new Vector3(), 2, new Color(255, 255, 255));
            NAPI.TextLabel.CreateTextLabel("Enter Medic HQ", enter_medicHQ, 10, 10, 1, new Color(255, 255, 255));
            entry.SetData("MedicHQ:Entry", new Vector3(275.446f, -1361.11f, 24.5378f));

            ColShape exit = NAPI.ColShape.CreateCylinderColShape(new Vector3(247.264f, -1371.894f, 24.53779f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(247.264f, -1371.894f, 24.53779f), new Vector3(), new Vector3(), 2, new Color(255, 255, 255));
            exit.SetData("MedicHQ:Exit", new Vector3(324.0096f, -554.3272f, 28.26531f));
        }

        [ServerEvent(Event.PlayerEnterColshape)]
        internal void EnterTownHall(ColShape colShape, Client client)
        {
            if (!client.GetPlayer(out var player)) return;

            if (colShape.HasData("MedicHQ:Entry") && !Enter & player.Team.Specialization == TeamSpecialization.Medic)
            {
                var entry = (Vector3)colShape.GetData("MedicHQ:Entry");
                client.Position = entry;
                Enter = true;
                NAPI.Task.Run(() =>
                {
                    Enter = false;
                }, delayTime: 2000);
            }
            if (colShape.HasData("MedicHQ:Exit") && !Enter & player.Team.Specialization == TeamSpecialization.Medic)
            {
                var exit = (Vector3)colShape.GetData("MedicHQ:Exit");
                client.Position = exit;
                Enter = true;
                NAPI.Task.Run(() =>
                {
                    Enter = false;
                }, delayTime: 2000);
            }
        }

        [Command("needmedic")]
        public void NeedMedic(Client client)
        {

            if (!client.GetPlayer(out var player)) return;

            client.SendChatMessage("Your sucessfull call an Medic, please wait...");
            client.SetData("CallMedic", null);

            foreach (Client targets in NAPI.Pools.GetAllPlayers())
            {
                if (targets.GetFromList().Team.Specialization == TeamSpecialization.Medic)
                {
                    targets.SendChatMessage(
                        $"Player {client.Name} needs a medic, type /acceptmedic to get the job.");
                    targets.SetData("RecieveMedic", null);
                }
            }
        }

        [Command("acceptmedic")]
        public void AcceptMedic(Client client, string playername)
        {

            if (!client.GetPlayer (out var player)) return;

            var reciever = NAPI.Pools.GetAllPlayers().SingleOrDefault(a => a.Name == playername);

            if (reciever.HasData("CallMedic"))
            {
                reciever.SendChatMessage("An medic is on the way, please wait...");
                reciever.ResetData("CallMedic");
            }
            else
            {
                return;
            }

            if (client.HasData("RecieveMedic"))
            {
                client.TriggerEvent("MedicFindPlayers", reciever.Position);
                client.SendChatMessage("You accepted the job, watch your Route");
                client.ResetData("RecieveMedic");
            }
            else
            {
                return;
            }
        }

        [Command("heal")]
        public void HealPlayer(Client client, string username)
        {
            if (!client.GetPlayer(out var player)) return;

            var targetplayer = NAPI.Pools.GetAllPlayers().Find(a => a.Name == username);

            if (player.PlayerInfo.SelectedTeam == TeamEnum.Civilian && player.Team.Specialization == TeamSpecialization.Medic)
            {
                if (targetplayer.Health == 100)
                {

                    client.SendChatMessage("The target has allready full life");
                    targetplayer.SendChatMessage("You cannot be health");
                    return;
                }

                targetplayer.SetData("AcceptHelp", null);
                targetplayer.SendChatMessage($"{client.Name} wants to heal you.Type /acceptheal to get heal.");
            }
            else
            {
                client.SendChatMessage("You cannot heal off duty");

            }
        }

        [Command("acceptheal")]
        public void AcceptHeal(Client client)
        {
            if (client.HasData("AcceptHelp"))
            {
                client.Health = 100;
                client.SendChatMessage($"You got health.");
                client.ResetData("AcceptHelp");
            }
        }
    }
}
