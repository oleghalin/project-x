﻿using GTANetworkAPI;
using System;
using MainResource.Enums;
using MainResource.Models.PlayerData;
using MainResource.Events;
using MainResource.Teams;
using static MainResource.Utility.ChatColors;
using MainResource.Handler;

namespace MainResource.Features.Professions.Civilian.Trucker.TruckerJob
{
    internal class TruckerJobSpawns {

        internal Vector3 colShapeVector;
        internal Vector3 truckSpawnVector;
        internal Vector3 trailerSpawnVector;
        internal float truckRotation;
        internal float trailerRotation;

        internal TruckerJobSpawns() { }

        internal TruckerJobSpawns(Vector3 colShapeVector, Vector3 truckSpawnVector, Vector3 trailerSpawnVector, float truckRotation, float trailerRotation)
        {
            this.truckRotation = truckRotation;
            this.trailerRotation = trailerRotation;
            this.colShapeVector = colShapeVector;
            this.truckSpawnVector = truckSpawnVector;
            this.trailerSpawnVector = trailerSpawnVector;
            ColShape colShape = NAPI.ColShape.CreateCylinderColShape(colShapeVector, 1, 1);
            NAPI.TextLabel.CreateTextLabel("Press [E] to open", colShapeVector, 1, 1, 1, new Color(255, 255, 255));
            colShape.SetData("UI", "trucker:Open");
            colShape.SetData("TruckerMain", JobIDs.TruckerJob);
            colShape.SetData("TruckLocation", truckSpawnVector);
            colShape.SetData("TruckRotation", truckRotation);
            colShape.SetData("TrailerLocation", trailerSpawnVector);
            colShape.SetData("TrailerRotation", trailerRotation);
        }
    }

    internal class TruckerJob : ServerEvents
    {

        static TeamEnum TeamRequired = TeamEnum.Civilian;
        static TeamSpecialization Specialization = TeamSpecialization.Trucker;

        internal Vehicle truck;
        internal Vehicle trailer;
        internal uint Reward { get; set; } = 0;

        internal override void OnResourceStart()
        {
            new TruckerJobSpawns(new Vector3(803.7892f, -1666.946f, 30.86441f), new Vector3(823.8515f, -1657.204f, 29.34675f), new Vector3(813.3762f, -1663.623f, 29.41401f), 350.3286f, 279.4666f);
            new TruckerJobSpawns(new Vector3(541.2504f, -1934.19f, 24.5028f), new Vector3(571.4778f, -1922.459f, 24.71271f), new Vector3(551.3829f, -1928.895f, 24.80783f), 119.7794f, 300.0361f);
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (player.PlayerInfo.SelectedTeam == TeamEnum.None) return;
            if (colShape.HasData("UI") && !player.Client.IsInVehicle && player.PlayerInfo.SelectedTeam == TeamRequired && player.Team.Specialization == Specialization)
            {
                player.Client.SetData("eDown:UI", colShape.GetData("UI"));
                player.Client.SetData("TruckerCol", colShape);
            }

            if (colShape.HasData("TruckerDestination") && player.Client.Vehicle.Trailer == trailer && player.PlayerInfo.SelectedTeam == TeamRequired && player.Team.Specialization == Specialization)
            {
                Finished(player, true, $"Delivery completed. You earned: ~g~${Convert.ToInt32(Reward)}~w~.");
                player.GiveExp(500);
                var col = (ColShape)colShape.GetData("TruckerDestination");
                col.Delete();
            }
        }

        [RemoteEvent("TruckerSelectJob")]
        internal void TruckerSelectJob(Client client, float x, float y, float z, int reward, string illegal)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Job != 0) return;
            Reward = Convert.ToUInt32(reward);
            if (illegal == "Yes")
            {
                player.PlayerInfo.Wanted += 2;
                UpdateHUD.UpdateWanted(player.Client, player.PlayerInfo.Wanted);
            }
            var vector = new Vector3(x, y, z);
            ColShape col = NAPI.ColShape.CreateCylinderColShape(vector, 2, 2);
            col.SetData("TruckerDestination", col);
            var colShape = (ColShape)player.Client.GetData("TruckerCol");
            Start(colShape, player);
            player.Job = JobIDs.TruckerJob;
            client.SendNotification("[~y~TRUCKER~w~]: Started delivery. Bring your cargo to the location in your minimap.");
        }

        internal override void Start(ColShape colShape, Player player)
        {
            var truckLoca = colShape.GetData("TruckLocation");
            var truckRota = colShape.GetData("TruckRotation");
            var trailerLoca = colShape.GetData("TrailerLocation");
            var trailerRota = colShape.GetData("TrailerRotation");
            player.Client.TriggerEvent("TruckerDelete");
            trailer = NAPI.Vehicle.CreateVehicle(VehicleHash.Trailers3, trailerLoca, trailerRota, 50, 50);
            truck = NAPI.Vehicle.CreateVehicle(VehicleHash.Phantom, truckLoca, truckRota, 50, 50);
            truck.SetData("TruckerVehicle", player.ID);
            trailer.SetData("TruckerTrailer", player.ID);
            player.Client.SetIntoVehicle(truck, -1);
            player.Client.TriggerEvent("trucker:Close");
        }

        internal override void Finished(Player player, bool completed, string reason = "")
        {
            if (completed)
            {
                player.Client.SendNotification($"~g~Delivery completed~w~: You earned ~g~${Convert.ToInt32(Reward)}~w~.");
                player.Client.TriggerEvent("trucker:Spawn");
                player.Client.TriggerEvent("trucker:Complete");
                DeleteTrailerAndTrailer(player.Client);
                player.AddCredits(Reward);
                player.Job = 0;
            }
            if (!completed)
            {
                player.Client.SendNotification($"~r~Delivery failed~w~: {reason}");
                player.Client.TriggerEvent("trucker:Spawn");
                player.Client.TriggerEvent("trucker:Complete");
                DeleteTrailerAndTrailer(player.Client);
                player.Job = 0;
            }
        }

        internal void DeleteTrailerAndTrailer(Client client) {
            if (!client.GetPlayer(out var player)) return;
            var trailer_id = trailer.GetData("TruckerTrailer");
            var vehicle_id = truck.GetData("TruckerVehicle");

            if (!trailer.Exists) return;
            if (trailer.Exists && trailer_id == player.ID) {
                trailer.Delete();
            }

            if (!truck.Exists) return;

            if (truck.Exists && vehicle_id == player.ID)
            {
                truck.Delete();
            }
        }
        [RemoteEvent("IsNearbyTrailer")]
        internal void IsNearbyTrailer(Client client) {
            if (!client.GetPlayer(out var player)) return;
            if (player.Job == 0) return;
            if (!trailer.Exists) return;

            if (client.Position.DistanceTo2D(trailer.Position) > 50 && trailer.HasData("TruckerTrailer") && player.Job == JobIDs.TruckerJob)
            {
                Finished(player, false, "You left your trailer!");
            }
        }

        [ServerEvent(Event.PlayerExitVehicleAttempt)]
        internal void OnPlayerExitVehicle(Client client, Vehicle veh) {
            if (!client.GetPlayer(out var player)) return;
            if (veh.HasData("TruckerVehicle") && player.Job == JobIDs.TruckerJob) {
            player.Client.SendNotification("~r~Get back into your vehicle or it will be deleted!");
                NAPI.Task.Run(() => {
                    if (player.Client.Vehicle == truck) return;
                    Finished(player, false, "You left your truck!");
                }, delayTime: 10000);
            }
        }
    }
}
