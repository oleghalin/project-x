﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using MainResource.Enums;
using System;
using System.Timers;
using MainResource.Teams;
using MainResource.Handler;
using MainResource.Classes;

namespace MainResource.Features.Professions.Criminal.StoreRob
{
    internal class StoreRob : ServerEvents
    {

        static TeamEnum TeamRequired = TeamEnum.Criminal;

        static Timer aTimer;

        internal override void OnResourceStart()
        {
            new Store("Robs Liqour", 100, new Vector3(-1487.657f, -379.314f, 40.16343f));
            new Store("Robs Liqour", 100, new Vector3(-2968.353f, 391.0988f, 15.04331f));
            new Store("24/7 Supermarket", 100, new Vector3(26.38923f, -1347.312f, 29.49703f));
            new Store("Ltd Gasoline", 100, new Vector3(-1821.483f, 793.0137f, 138.124f));
            new Store("Ltd Gasoline", 100, new Vector3(1162.889f, -323.0853f, 69.20508f));
            new Store("Logger Bar", 100, new Vector3(1982.573f, 3053.242f, 47.21507f));
            new Store("Ltd Gasoline", 100, new Vector3(1699.148f, 4924.482f, 42.06364f));
            new Store("24/7 Supermarket", 100, new Vector3(1729.372f, 6414.442f, 35.03722f));
            new Store("24/7 Supermarket", 100, new Vector3(547.4503f, 2671.18f, 42.1565f));
            new Store("Robs Liqour", 100, new Vector3(1165.989f, 2708.864f, 38.1577f));
            new Store("24/7 Supermarket", 100, new Vector3(2557.273f, 382.7008f, 108.6229f));
            new Store("24/7 Supermarket", 100, new Vector3(374.3918f, 326.1013f, 103.5664f));
        }

        internal static void StartRobbing(ColShape colShape, Player player)
        {
            if (colShape.HasData("Store"))
            {
                var store = (Store)colShape.GetData("Store");

                if ((DateTime.UtcNow - store.LastRobbed).TotalMinutes < 1)
                {
                    player.SendNotification("Store", "This store was already robbed recently!", "warning");
                    return;
                }
                else if (store.Robbing)
                {
                    player.SendNotification("Store", "Someone is already robbing this store!", "warning");
                    return;
                }
                else
                {
                    player.Client.SetData("Store", store);
                    SetTimer(player, colShape);
                    store.Robbing = true;
                    player.Job = JobIDs.StoreRobbery;
                    player.Client.TriggerEvent("DisableFiring", true);
                    player.Client.PlayAnimation("random@atmrobberygen", "a_atm_mugging", (int)(AnimationFlags.Loop | AnimationFlags.AllowPlayerControl | AnimationFlags.OnlyAnimateUpperBody));
                    player.SendNotification("Store", $"Started robbing {store.StoreName}", "success");
                    player.AddWanted(2);
                }
            }
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("Store") && player.PlayerInfo.SelectedTeam == TeamRequired && !player.Client.IsInVehicle && player.Job == 0)
            {
                player.Client.SetData("eDown:Action", Actions.StoreRob);
            }
        }

        internal static void FinishedRobbing(Player player, bool completed, string reason = "")
        {
            if (player.Client.HasData("Store"))
            {
                var store = (Store)player.Client.GetData("Store");

                if (completed)
                {
                    player.Client.TriggerEvent("DisableFiring", false);
                    player.Client.StopAnimation();
                    store.Robbing = false;
                    store.LastRobbed = DateTime.UtcNow;
                    player.SendNotification("Store","Robbery completed: Succesfully robbed the store!", "success");
                    RobRewards(player, completed);
                    player.Job = 0;
                    aTimer.Dispose();
                }
                if (!completed)
                {
                    player.Client.TriggerEvent("DisableFiring", false);
                    player.Client.StopAnimation();
                    aTimer.Dispose();
                    store.Robbing = false;
                    player.SendNotification("Store", $"Robbery failed: {reason}", "danger");
                    player.Job = 0;
                }

                player.Client.ResetData("Store");
            }
        }
        
        internal static void RobRewards(Player player, bool completed)
        {
            var store = (Store)player.Client.GetData("Store");

            if (completed == true && store.Robbing == false)
            {
                player.AddCredits(Convert.ToUInt32(store.CashRegisterWorth * 2));

                foreach (Client client in NAPI.Pools.GetAllPlayers())
                {
                    if (!client.GetPlayer(out var otherPlayer)) return;
                    if (otherPlayer.PlayerInfo.SelectedTeam == TeamEnum.Government)
                    {
                        otherPlayer.SendNotification("Police Radio", $"A Store got robbed by {player.PlayerInfo.PlayerName}", "primary");
                    }
                }
            }
        }

        internal override void OnPlayerExitColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("Store") && player.Client.HasData("Store"))
            {
                var store = (Store)player.Client.GetData("Store");
                if (store.Robbing)
                {
                    FinishedRobbing(player, false, "You left the robbery!");
                    return;
                }

                player.Client.ResetData("Store");
            }
        }

        internal static void SetTimer(Player player, ColShape colShape)
        {
            aTimer = new Timer(10000);
            aTimer.Elapsed += (sender, e) => OnTimedEvent(sender, e, player, colShape);
            aTimer.Enabled = true;
        }

        internal static void OnTimedEvent(object source, ElapsedEventArgs e, Player player, ColShape colShape)
        {
            FinishedRobbing(player, true);
        }
    }
}
