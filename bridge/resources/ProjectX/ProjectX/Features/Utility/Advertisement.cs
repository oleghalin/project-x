﻿using MainResource.Handler;
using GTANetworkAPI;
using System.Timers;
using MainResource.Models.PlayerData;
using System.Collections.Generic;

namespace MainResource.Features.Utility
{
    internal class Advertisement : Script
    {
        static List<(Client, string)> Messages = new List<(Client, string)>();
        static Timer Timer = new Timer();


        internal static void AddAdvertisement(Client client, string message)
        {
            foreach(var (c, msg) in Messages)
            {
                if (c == client)
                {
                    client.SendNotification($"You already have an advertisement in queue");
                    return;
                }
            }
            if (Messages.Count == 0) StartTimer();
            Messages.Add((client, message));
        }

        internal static void StartTimer()
        {
            Timer.Interval = 5000;
            Timer.Elapsed += (sender, e) => timerElapsed(sender, e);
            Timer.Start();
        }

        internal static void timerElapsed(object sender, ElapsedEventArgs e)
        {
            if (Messages.Count != 0)
            {
                NAPI.Notification.SendNotificationToAll(Messages[0].Item2);
                Messages.Remove(Messages[0]);
                if (Messages.Count == 0) Timer.Close();
            }        
        }
    }

    internal class AdvertisementCenter : ServerEvents
    {
        internal override void OnResourceStart()
        {
            ColShape col = NAPI.ColShape.CreateCylinderColShape(new Vector3(413.3622f, -989.0004f, 29.41637f), 2, 2);
            col.SetData("AdvertisementCenter", null);
            NAPI.Marker.CreateMarker(1, new Vector3(413.3622f, -989.0004f, 29.41637f - 1f), new Vector3(), new Vector3(), 2, new Color(255, 255, 0));
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("AdvertisementCenter"))
            {
                player.Client.SetData("InAdvertisementCenter", null);
            }
        }

        internal override void OnPlayerExitColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("AdvertisementCenter") && player.Client.HasData("InAdvertisementCenter"))
            {
                player.Client.ResetData("InAdvertisementCenter");
            }
        }

        [Command("ad", GreedyArg = true)]
        internal void QueueAdvertisement(Client client, string message)
        {
            if (!client.GetPlayer(out var player)) return;
            if (client.HasData("InAdvertisementCenter") && message != null)
            {
                Advertisement.AddAdvertisement(client, message);
                player.TakeCredits(100);
            }
        }
    }
}
