﻿using GTANetworkAPI;
using MainResource.Features.Professions.Criminal;
using MainResource.Features.Professions.Criminal.StoreRob;
using MainResource.Models.PlayerData;

namespace MainResource.Enums
{
    public enum Actions
    {
        None = 0,
        StoreRob = 1,
        PropertyRob = 2
    }

    internal class Action
    {
        internal static void GetAction(Client client, Actions action)
        {
            if (!client.GetPlayer(out var player)) return;
            
            switch ((int)action)
            {
                case 1:
                    if (player.ColShape == null) return;
                    StoreRob.StartRobbing(player.ColShape, player);
                        break;
                case 2:
                    if (player.ColShape == null) return;
                    PropertyRob.StartRobbing(player.ColShape, player);
                    break;
                default:
                    break;
            }
        }
    }
}
