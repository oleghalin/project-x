﻿using GTANetworkAPI;
using System;
using System.Timers;

namespace MainResource.Classes
{
    internal class Store
    {
        internal string StoreName { get; private set; }
        internal uint CashRegisterWorth { get; set; }
        internal bool Robbing { get; set; } = false;
        internal DateTime LastRobbed { get; set; } = DateTime.UtcNow;
        internal Store(string storeName, uint worth, Vector3 storeLocation)
        {
            StoreName = storeName;
            CashRegisterWorth = worth;
            ColShape col = NAPI.ColShape.CreateCylinderColShape(storeLocation, 1, 1);
            col.SetData("Store", this);
        }
    }

    internal class StorePeds : Script
    {
        internal StorePeds()
        {
            
        }
    }
}
