﻿using GTANetworkAPI;
using System.Collections.Generic;

namespace MainResource.Classes
{
    public static class Hospital
    {
        public static List<(Vector3, float)> Hospitals = new List<(Vector3, float)>()
        {
            (new Vector3(-449.4493f, -341.0733f, 34.50176f), 78.64225f),
            (new Vector3(1839.369f, 3672.729f, 34.27672f), 214.9673f),
            (new Vector3(-247.6842f, 6331.134f, 32.42616f), 224.4187f),
            (new Vector3(298.4828f, -584.5302f, 43.26084f), 68.43027f)
        };
    }
}
