﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;

namespace MainResource.Classes
{
    internal class Loot
    {
        internal string Name { get; set; }
        internal uint Worth { get; set; }
        internal Client Owner { get; set; }

        internal Loot(string name, uint worth, Client client)
        {
            Name = name;
            Worth = worth;
            Owner = client;
        }

        internal static void RemoveLoot(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.VehicleInfo.Vehicle != null && player.VehicleInfo.Inventory.Count > 0)
            {
                player.VehicleInfo.Inventory.Clear();
            }
        }
    }
}
