﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Timers;

namespace MainResource.Classes
{
    internal class Property
    {
        internal string PropertyName { get; set; }
        internal List<Vector3> ObjectSpawns { get; set; }
        internal DateTime LastRobbed { get; set; } = DateTime.UtcNow;
        internal Property(string propertyName, Vector3 storeLocation, List<Vector3> objectSpawns)
        {
            PropertyName = propertyName;
            ObjectSpawns = objectSpawns;
            ColShape col = NAPI.ColShape.CreateCylinderColShape(storeLocation, 2, 2);
            col.SetData("Property", this);
            NAPI.Blip.CreateBlip(1, storeLocation, 1, 1);
        }
    }
}
