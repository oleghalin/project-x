﻿using MainResource.Models.PlayerData;
using System.Collections.Generic;
using GTANetworkAPI;
using System;
using MainResource.Handler;
using System.Linq;

namespace MainResource.Classes.Missions
{
    internal class Mission : ServerEvents
    {
        internal Guid ID { get; set; } = Guid.NewGuid();
        internal List<Objective> Objectives { get; set; } = new List<Objective>();
        internal List<Player> Players { get; set; } = new List<Player>();
        internal uint MaxObjectives { get; set; } = 2;
        internal Objective CurrentObjective { get; set; } = null;

        [Command("startmission")]
        internal void StartMission(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Party != null)
            {
                if (player.Party.Leader != player)
                {
                        player.SendNotification("Start Mission", "Only the leader of this party can start a mission", "warning");
                        return;
                }
                Mission mission = new Mission();
                player.Mission = mission;
                foreach (var partyPlayer in player.Party.Players)
                {
                    if (partyPlayer == player) continue;
                    partyPlayer.Mission = mission;
                    player.Mission.Players.Add(partyPlayer);
                }
                mission.StartMission(player);
            }
            else if (player.Party == null)
            {
                Mission mission = new Mission();
                player.Mission = mission;
                mission.StartMission(player);
            }
        }

        internal void StartMission(Player player)
        {
            Players.Add(player);
            Objectives.Add(new Objective().NewObjective(Enums.Objectives.Robbery));
            Objectives.Add(new Objective().NewObjective(Enums.Objectives.Vandalism));
            SpawnObjective();
            player.SendNotification("Mission", "Mission started", "success");
        }

        public void SpawnObjective()
        {
            CurrentObjective = Objectives[Global.Random.Next(0, Objectives.Count)];
            foreach (var player in Players)
            {
                CurrentObjective.Spawn(player);
                player.Client.TriggerEvent("Mission:Spawn", CurrentObjective.Location);
            }
            Objectives.Remove(CurrentObjective);
        }

        public void FinishMission()
        {
            foreach(var player in Players)
            {
                if (player.Client.HasData("Mission:Rob") || player.Client.HasData("Mission:Van"))
                {
                    player.Client.TriggerEvent("Progress:Stop");
                }
                player.SendNotification("Mission", "Succesfully completed the mission", "success");
            }
        }

        public void ResetPlayerDatas(string key)
        {
            foreach(var player in Players)
            {
                player.Client.ResetData(key);
            }
        }

        public void SendNotificationToPlayers(string title, string message, string variant)
        {
            foreach(var player in Players)
            {
                player.SendNotification(title, message, variant);
            }
        }

        public void NextMission()
        {
            if (Objectives.Count >= 1)
            {
                foreach(var player in Players)
                {
                    player.Client.TriggerEvent("Mission:Delete");
                }
                SpawnObjective();
            }
            else
            {
                foreach (var player in Players)
                {
                    player.Client.TriggerEvent("Mission:Delete");
                }
                FinishMission();
            }
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (player.Mission != null)
            {
                if (player.Mission.Players.Contains(player))
                {
                    if (colShape.HasData("Mission:Van") && player.Client.HasData("Mission:Van"))
                    {
                        player.Mission.CurrentObjective.ObjTrigger(player);
                    }
                    else if (colShape.HasData("Mission:Rob") && player.Client.HasData("Mission:Rob"))
                    {
                        player.Mission.CurrentObjective.ObjTrigger(player);
                    }
                }
            }
            
        }

        internal override void OnPlayerExitColShape(ColShape colShape, Player player)
        {
            if (player.Client.HasData("Mission:Rob"))
            {
                if (player.Mission.CurrentObjective != null)
                {
                    player.Mission.CurrentObjective.Robbery.Robbing = false;
                }
                player.Client.TriggerEvent("Progress:Stop");
            }
            if (player.Client.HasData("Mission:Van"))
            {
                if (player.Mission.CurrentObjective != null)
                {
                    player.Mission.CurrentObjective.Vandalism.Vandalising = false;
                }
                player.Client.TriggerEvent("Progress:Stop");
            }
        }
    }
}
