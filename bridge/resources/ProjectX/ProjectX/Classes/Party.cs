﻿using GTANetworkAPI;
using MainResource.Handler;
using MainResource.Models.PlayerData;
using System.Collections.Generic;
using System.Linq;

namespace MainResource.Classes
{
    internal class Party : ServerEvents
    {
        public List<Player> Players { get; set; } = new List<Player>();
        public Player Leader { get; set; }
        private readonly int maxMembers = 4;
        internal Party() { }
        internal Party(Player player)
        {
            Leader = player;
            Players.Add(player);
        }

        [Command("createparty")]
        internal void CreateParty(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Party == null)
            {
                player.Party = new Party(player);
                player.SendNotification("Party", "Succesfully created a party", "success");
                player.Client.TriggerEvent("Party:Draw", player.PlayerInfo.PlayerName);
            }
        }

        internal void UpdateParty(Player player)
        {
            foreach(var gamer in player.Party.Players)
            {
                var players = gamer.Party.Players;
                switch (players.Count)
                {
                    case 1:
                        gamer.Client.TriggerEvent("Party:Draw", players[0].PlayerInfo.PlayerName);
                        break;
                    case 2:
                        gamer.Client.TriggerEvent("Party:Draw", players[0].PlayerInfo.PlayerName, players[1].PlayerInfo.PlayerName);
                        break;
                    case 3:
                        gamer.Client.TriggerEvent("Party:Draw", players[0].PlayerInfo.PlayerName, players[1].PlayerInfo.PlayerName, players[2].PlayerInfo.PlayerName);
                        break;
                    case 4:
                        gamer.Client.TriggerEvent("Party:Draw", players[0].PlayerInfo.PlayerName, players[1].PlayerInfo.PlayerName, players[2].PlayerInfo.PlayerName, players[3].PlayerInfo.PlayerName);
                        break;
                    default:
                        break;
                }
            }
        }

        [Command("pinvite")]
        internal void PartyInvite(Client client, Client invited)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Party != null)
            {
                if (player.Party.Leader != player)
                {
                    player.SendNotification("Create Mission", "Only the leader of this party can invite members", "warning");
                    return;
                }
                if (player.Party.Players.Count == player.Party.maxMembers)
                {
                    player.SendNotification("Party", "Your party is full", "warning");
                    return;
                }
                if (!invited.GetPlayer(out var invitedPlayer))
                {
                    player.SendNotification("Party", $"User {invited.Name} does not exist", "danger");
                    return;
                }
                if (invitedPlayer.Party != null)
                {
                    player.SendNotification("Party Invite", "This user is already in another party", "warning");
                    return;
                }
                invitedPlayer.SendNotification("Party", $"You have been invited to {player.PlayerInfo.PlayerName}s party, /paccept or /pdecline", "secondary");
                invited.SetData("PartyInvite", player);
            }
        }

        [Command("paccept")]
        internal void PAccept(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Client.HasData("PartyInvite"))
            {
                var inviter = (Player)player.Client.GetData("PartyInvite");
                if (inviter.IsPlayerOnline())
                {
                    if (inviter.Party.Players.Count == inviter.Party.maxMembers)
                    {
                        player.SendNotification("Party Invite", $"{inviter.PlayerInfo.PlayerName}s party is full", "danger");
                        player.Client.ResetData("PartyInvite");
                        return;
                    }
                    inviter.Party.Players.Add(player);
                    player.Party = inviter.Party;
                    player.SendNotification("Party", "Joined party", "success");
                    inviter.SendNotification("Party Invite", $"User {player.PlayerInfo.PlayerName} has joined your party", "success");
                    UpdateParty(player);
                }
            }
        }

        [Command("pdecline")]
        internal void PDecline(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Client.HasData("PartyInvite"))
            {
                var inviter = (Player)player.Client.GetData("PartyInvite");
                if (inviter.IsPlayerOnline())
                {
                    player.Client.ResetData("PartyInvite");
                    player.SendNotification("Party Invite", "Declined invite", "warning");
                    inviter.SendNotification("Party Invite", $"User {player.PlayerInfo.PlayerName} declined your party invite", "warning");
                }
            }
        }

        [Command("leaveparty")]
        internal void LeaveParty(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Party != null)
            {
                if(player.Party.Players.Contains(player))
                {
                    player.Party.Players.Remove(player);
                    player.Party.UpdateParty(player);
                    player.Party = null;
                    player.Client.TriggerEvent("Party:StopDraw");
                    player.SendNotification("Party", "You have succesfully left your party", "warning");
                }
            }
        }

        [Command("pkick")]
        internal void PKick(Client client, Client kicked)
        {
            if (!client.GetPlayer(out var player)) return;
            if (!kicked.GetPlayer(out var kickedPlayer))
            {
                player.SendNotification("Party Kick", "This user does not exist", "danger");
                return;
            }
            if (player.Party != null)
            {
                if (player.Party.Leader != player)
                {
                    player.SendNotification("Create Mission", "Only the leader of this party can kick players", "warning");
                    return;
                }
                if (player.Party.Players.Contains(kickedPlayer))
                {
                    player.Party.Players.Remove(kickedPlayer);
                    kickedPlayer.Party = null;
                    kickedPlayer.SendNotification("Party", "You have been removed from party", "danger");
                    kickedPlayer.Client.TriggerEvent("Party:StopDraw");
                }
            }
        }
    }
}
