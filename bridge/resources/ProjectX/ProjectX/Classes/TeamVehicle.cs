﻿using GTANetworkAPI;
using MainResource.Enums;
using MainResource.Teams;

namespace MainResource.Classes
{
    public class TeamVehicle
    {
        public ulong VehicleHash { get; set; }
        public Vector3 Position { get; set; }
        public float Rotation { get; set; }
        public TeamEnum TeamEnum { get; set; }
        public TeamSpecialization Specialization { get; set; } = TeamSpecialization.None;

        public TeamVehicle(uint veh, TeamEnum t, TeamSpecialization s, Vector3 pos, float rot)
        {
            VehicleHash = veh;
            TeamEnum = t;
            Specialization = s;
            Position = pos;
            Rotation = rot;
            Global.TeamVehicles.Add(this);
            Global.AddListToDB();
        }
    }
}
