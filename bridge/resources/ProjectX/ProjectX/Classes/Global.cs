﻿using GTANetworkAPI;
using MainResource.Handler;
using MainResource.Models;
using System;
using System.Collections.Generic;

namespace MainResource.Classes
{
    internal class Global : ServerEvents
    {
        public static Random Random { get; private set; } = new Random();
        public static List<TeamVehicle> TeamVehicles { get; set; } = new List<TeamVehicle>();
        public static List<Vector3> VandalismCoords { get; set; } = new List<Vector3>()
        {
            new Vector3(-427.3232f, -42.0081f, 46.22668f),
            new Vector3(-439.6387f, 22.38646f, 46.01821f)
        };
        public static List<Vector3> RobberyCoords { get; set; } = new List<Vector3>()
        {
            new Vector3(-430.5469f, -23.99458f, 46.22869f),
            new Vector3(-425.0773f, 21.54348f, 46.26283f)
        };

        internal override async void OnResourceStart()
        {
            SpawnCols();
            TeamVehicles = await DatabaseHandler.GetTeamVehicles();
            if (TeamVehicles.Count != 0)
            {
                foreach (TeamVehicle veh in TeamVehicles)
                {
                    NAPI.Task.Run(() =>
                    {
                        var vehicle = NAPI.Vehicle.CreateVehicle((uint)veh.VehicleHash, veh.Position, veh.Rotation, 0, 0);
                        vehicle.EngineStatus = false;
                        vehicle.SetData("TeamVehicle", veh);
                    });
                }
            }
        }

        public void SpawnCols()
        {
            for (int i = 0; i < RobberyCoords.Count; i++)
            {
                var col = NAPI.ColShape.CreateCylinderColShape(RobberyCoords[i], 2, 2);
                col.SetData("Mission:Rob", i);
            }
            for (int i = 0; i < VandalismCoords.Count; i++)
            {
                var col = NAPI.ColShape.CreateCylinderColShape(VandalismCoords[i], 2, 2);
                col.SetData("Mission:Van", i);
            }
        }

        public static void AddListToDB()
        {
            DatabaseHandler.AddTeamVehicles(TeamVehicles);
        }
    }
}
