﻿using GTANetworkAPI;
using MainResource.Enums;
using MainResource.Models.PlayerData;

namespace MainResource.Teams
{
    public class Team
    {
        public string TeamName { get; set; } = "None";
        public int BlipColor { get; set; } = 0;
        public ulong EXP { get; set; } = 0;
        public uint Level { get; set; } = 0;
        public string HexColor { get; set; } = "#ffffff";
        public TeamEnum TeamEnum { get; set; } = TeamEnum.None;
        public TeamSpecialization Specialization { get; set; } = TeamSpecialization.None;

        internal Team SetTeam(Client client, TeamEnum teamenum)
        {
            if (!client.GetPlayer(out var player)) return null;
            switch (teamenum)
            {
                case TeamEnum.Civilian:
                    SetTeamCivilian();
                    player.PlayerInfo.SelectedTeam = TeamEnum.Civilian;
                    client.SendNotification($"You have joined ~y~[CIVILIAN]");
                    return this;
                case TeamEnum.Criminal:
                    SetTeamCriminal();
                    player.PlayerInfo.SelectedTeam = TeamEnum.Criminal;
                    client.SendNotification($"You have joined ~r~[CRIMINAL]");
                    return this;
                case TeamEnum.Government:
                    SetTeamGovernment();
                    player.PlayerInfo.SelectedTeam = TeamEnum.Government;
                    client.SendNotification($"You have joined ~b~[GOVERNMENT]");
                    return this;
                default:
                    return null;
            }
        }

        internal void GetTeamSpecialization(Client client, TeamSpecialization s)
        {
            if (!client.GetPlayer(out var player)) return;

            switch (s)
            {
                case TeamSpecialization.Arms_Dealer:
                    break;
                case TeamSpecialization.Drug_Dealer:
                    break;
                case TeamSpecialization.Offense_Specialist:
                    Government.Government.GetGear(client, TeamSpecialization.Offense_Specialist);
                    break;
                case TeamSpecialization.Defense_Specialist:
                    Government.Government.GetGear(client, TeamSpecialization.Defense_Specialist);
                    break;
            }
        }

        internal void SetTeamCivilian()
        {
            TeamName = "Civilian";
            BlipColor = 5;
            HexColor = "#f4d742";
            TeamEnum = TeamEnum.Civilian;
        }

        internal void SetTeamCriminal()
        {
            TeamName = "Criminal";
            BlipColor = 76;
            HexColor = "#77180e";
            TeamEnum = TeamEnum.Criminal;
        }

        internal void SetTeamGovernment()
        {
            TeamName = "Government";
            BlipColor = 29;
            HexColor = "#0e4d77";
            TeamEnum = TeamEnum.Government;
        }
    }

    public enum TeamEnum
    {
        None = 0,
        Civilian = 1,
        Criminal = 2,
        Government = 3
    }
}
