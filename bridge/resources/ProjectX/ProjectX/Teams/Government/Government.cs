﻿using GTANetworkAPI;
using MainResource.Handler;
using MainResource.Models.PlayerData;
using System.Collections.Generic;
using MainResource.Enums;

namespace MainResource.Teams.Government
{
    internal class Government : ServerEvents
    {
        internal Government()
        {
            ColShape colShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(447.2827f, -976.331f, 30.68958f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(447.2827f, -976.331f, 30.68958f - 0.8f), new Vector3(), new Vector3(), 2, new Color(0, 0, 200));
            NAPI.TextLabel.CreateTextLabel("Type /government to join Government", new Vector3(447.2827f, -976.331f, 30.68958f), 1, 1, 1, new Color(255, 255, 255));
            NAPI.Blip.CreateBlip(60, new Vector3(447.2827f, -976.331f, 30.68958f), 0.6f, 29, "Police HQ", 255, 50, true);
            colShape.SetData("HQ", null);
            ColShape armory = NAPI.ColShape.CreateCylinderColShape(new Vector3(452.0917f, -980.1563f, 30.68961f), 1, 1);
            armory.SetData("Armory", null);
            ColShape garage = NAPI.ColShape.CreateCylinderColShape(new Vector3(445.8282f, -996.3215f, 30.68959f), 1, 1);
            garage.SetData("PoliceGarage", null);
            ColShape helipad = NAPI.ColShape.CreateCylinderColShape(new Vector3(456.7452f, -987.4859f, 43.69164f), 1, 1);
            helipad.SetData("PoliceHelipad", null);
        }

        static List<(WeaponHash, int)> RookieGear = new List<(WeaponHash, int)>()
        {
            (WeaponHash.AdvancedRifle, 100),
            (WeaponHash.CombatPistol, 50),
            (WeaponHash.Nightstick, 1),
            (WeaponHash.StunGun, 20)
        };
        static List<(WeaponHash, int)> DefenseGear = new List<(WeaponHash, int)>()
        {
            (WeaponHash.AssaultRifle, 250),
            (WeaponHash.BullpupShotgun, 100),
            (WeaponHash.NightVision, 1),
            (WeaponHash.SmokeGrenade, 20),
            (WeaponHash.Nightstick, 1),
            (WeaponHash.StunGun, 20)
        };
        static List<(WeaponHash, int)> OffenseGear = new List<(WeaponHash, int)>()
        {
            (WeaponHash.SpecialCarbine, 350),
            (WeaponHash.HeavyPistol, 150),
            (WeaponHash.BZGas, 20),
            (WeaponHash.SMG, 300),
            (WeaponHash.Nightstick, 1),
            (WeaponHash.StunGun, 20)
        };


        [Command("government")]
        internal void JoinGovernment(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (client.HasData("HQ"))
            {
                if (player.PlayerInfo.SelectedTeam == TeamEnum.Government)
                {
                    client.SendNotification("You are already part of ~b~[GOVERNMENT]");
                    return;
                }
                if (player.PlayerInfo.Level < 2)
                {
                    client.SendNotification("You have to be ~y~Level 2~w~ or higher to join ~b~[GOVERNMENT]");
                    return;
                }
                if (player.PlayerInfo.SelectedTeam == TeamEnum.Criminal)
                {
                    client.SendNotification("You cannot join ~b~[GOVERNMENT]~w~ while you are ~r~[CRIMINAL]");
                    return;
                }
                if (player.PlayerInfo.Wanted > 0)
                {
                    client.SendNotification("Your wanted level is too high to join ~b~[GOVERNMENT]");
                    return;
                }
                if (player.PlayerInfo.GetPlayerTeam(TeamEnum.Criminal) != null)
                {
                    client.SendNotification($"You are already part of team ~r~[CRIMINAL]~w~, are you sure you want to change?");
                    client.SendNotification($"/acceptchange or /declinechange");
                    client.SetData("TeamChange", TeamEnum.Government);
                    return;
                }
                if (player.PlayerInfo.GetPlayerTeam(TeamEnum.Government) != null)
                {
                    player.PlayerInfo.SelectedTeam = TeamEnum.Government;
                    player.PlayerInfo.SetPlayerSelectedTeam(player);
                    return;
                }
                player.PlayerInfo.PlayerJoinTeam(client, TeamEnum.Government);
            }
        }

        [Command("declinechange")]
        internal void DeclineChange(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (client.HasData("TeamChange"))
            {
                client.ResetData("TeamChange");
                client.SendNotification($"Declined team change");
            }
        }

        [Command("acceptchange")]
        internal void AcceptChange(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (client.HasData("TeamChange"))
            {
                var teamToChange = (TeamEnum)client.GetData("TeamChange");
                player.PlayerInfo.PlayerChangeTeam(player, teamToChange);
            }
        }

        [Command("defense")]
        internal void Defense(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Client.HasData("Armory") && player.Team.Specialization == TeamSpecialization.None)
            {
                player.Team.Specialization = TeamSpecialization.Defense_Specialist;
                GetGear(client, player.Team.Specialization);
                player.SendNotification("Armory", "Assigned as Defense Specialist", "success");
            }
        }

        [Command("offense")]
        internal void Offense(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Client.HasData("Armory") && player.Team.Specialization == TeamSpecialization.None)
            {
                player.Team.Specialization = TeamSpecialization.Offense_Specialist;
                GetGear(client, player.Team.Specialization);
                player.SendNotification("Armory", "Assigned as Offense Specialist", "success");
            }
        }

        internal static void GetGear(Client client, TeamSpecialization s)
        {
            if (!client.GetPlayer(out var player)) return;

            switch (s)
            {
                case TeamSpecialization.Offense_Specialist:
                    foreach ((var gun, var ammo) in OffenseGear)
                    {
                        player.Client.GiveWeapon(gun, ammo);
                    }
                    break;
                case TeamSpecialization.Defense_Specialist:
                    foreach ((var gun, var ammo) in DefenseGear)
                    {
                        player.Client.GiveWeapon(gun, ammo);
                    }
                    break;
                default:
                    break;
            }
        }

        [Command("pheli")]
        internal void GetHeli(Client client, string vehicle)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Client.HasData("PoliceHelipad"))
            {
                if (vehicle != null)
                {
                    player.PlayerInfo.DespawnVehicle(player);
                    switch (vehicle)
                    {
                        case "maverick":
                            var maverick = NAPI.Vehicle.CreateVehicle(VehicleHash.Polmav, new Vector3(449.5631f, -981.257f, 43.69167f), 86.52309f, 0, 0);
                            player.Client.SetIntoVehicle(maverick, -1);
                            if (player.VehicleInfo.Vehicle != null) player.PlayerInfo.DespawnVehicle(player);
                            break;
                        default:
                            player.SendNotification("Police Garage", "Wrong vehicle entered, try again", "danger");
                            break;
                    }
                }
            }
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("HQ"))
            {
                player.Client.SetData("HQ", null);
            }

            if (colShape.HasData("Armory") && player.Team.TeamEnum == TeamEnum.Government)
            {
                player.SendNotification("Specialization", "Select a specialization: /offense or /defense", "secondary");
                player.Client.SetData("Armory", null);
            }

            if (colShape.HasData("PoliceGarage") && player.Team.TeamEnum == TeamEnum.Government)
            {
                player.SendNotification("Police Garage", "Choose a vehicle: police, policeunmarked, policebike, policetransport, policeriot or parkranger. To spawn it, type /pveh vehiclename", "primary");
                player.Client.SetData("PoliceGarage", null);
            }

            if (colShape.HasData("PoliceHelipad") && player.Team.TeamEnum == TeamEnum.Government)
            {
                player.SendNotification("Police Helipad", "Choose a helicopter: maverick. To spawn it, type /pheli vehiclename", "primary");
                player.Client.SetData("PoliceHelipad", null);
            }
        }

        internal override void OnPlayerExitColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("HQ") && player.Client.HasData("HQ"))
            {
                player.Client.ResetData("HQ");
            }

            if (colShape.HasData("Armory") && player.Client.HasData("Armory"))
            {
                player.Client.ResetData("Armory");
            }

            if (colShape.HasData("PoliceGarage") && player.Client.HasData("PoliceGarage"))
            {
                player.Client.ResetData("PoliceGarage");
            }

            if (colShape.HasData("PoliceHelipad") && player.Client.HasData("PoliceHelipad"))
            {
                player.Client.ResetData("PoliceHelipad");
            }
        }
    }
}
