﻿using GTANetworkAPI;
using MainResource.Enums;
using MainResource.Handler;
using MainResource.Models.PlayerData;
using System;
using System.Collections.Generic;

namespace MainResource.Teams.Criminal
{
    internal class Criminal : ServerEvents
    {
        internal Criminal()
        {
            ColShape colShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(1394.377f, 3602.296f, 38.94194f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(1394.377f, 3602.296f, 38.94194f - 0.8f), new Vector3(), new Vector3(), 2, new Color(200, 0, 0));
            NAPI.TextLabel.CreateTextLabel("Type /criminal to join Criminals", new Vector3(1394.377f, 3602.296f, 38.94194f), 1, 1, 1, new Color(255, 255, 255));
            NAPI.Blip.CreateBlip(84, new Vector3(1394.377f, 3602.296f, 38.94194f), 0.6f, 76, "Criminal Safehouse", 255, 50, true);
            colShape.SetData("SafeHouse", null);
            ColShape specialization = NAPI.ColShape.CreateCylinderColShape(new Vector3(1395.335f, 3613.044f, 34.98093f), 1, 1);
            specialization.SetData("Specialization", null);
        }

        [Command("criminal")]
        internal void JoinCriminals(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (client.HasData("SafeHouse"))
            {
                if (player.PlayerInfo.SelectedTeam == TeamEnum.Criminal)
                {
                    client.SendNotification("You are already part of ~r~[CRIMINAL]");
                    return;
                }
                if (player.PlayerInfo.Level < 2)
                {
                    client.SendNotification("You have to be ~y~Level 2~w~ or higher to join ~r~[CRIMINAL]");
                    return;
                }
                if (player.PlayerInfo.SelectedTeam == TeamEnum.Government)
                {
                    client.SendNotification("You cannot join ~r~[CRIMINAL]~w~ while you are ~b~[GOVERNMENT]");
                    return;
                }
                if (player.PlayerInfo.GetPlayerTeam(TeamEnum.Government) != null)
                {
                    client.SendNotification($"You are already part of team ~b~[GOVERNMENT]~w~, are you sure you want to change?");
                    client.SendNotification($"/acceptchange or /declinechange");
                    client.SetData("TeamChange", TeamEnum.Criminal);
                    return;
                }
                if (player.PlayerInfo.GetPlayerTeam(TeamEnum.Criminal) != null)
                {
                    player.PlayerInfo.SelectedTeam = TeamEnum.Criminal;
                    player.PlayerInfo.SetPlayerSelectedTeam(player);
                    return;
                }
                player.PlayerInfo.PlayerJoinTeam(client, TeamEnum.Criminal);
            }
        }

        [Command("arms")]
        internal void Arms(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Client.HasData("Specialization") && player.Team.Specialization == TeamSpecialization.None)
            {
                player.Team.Specialization = TeamSpecialization.Arms_Dealer;
                player.SendNotification("Specialization", "Assigned as Arms Dealer", "success");
            }
        }

        [Command("sellguns")]
        internal void SellGuns(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Team.TeamEnum == TeamEnum.Criminal && player.Team.Specialization == TeamSpecialization.Arms_Dealer)
            {
                if (player.Client.HasData("SellGuns"))
                {
                    if (client.HasData("SellGunShape"))
                    {
                        var col = (ColShape)client.GetData("SellGunShape");
                        col.Delete();
                        client.ResetData("SellGuns");
                    }
                    if (client.HasData("SellGunMarker"))
                    {
                        var marker = (Marker)client.GetData("SellGunMarker");
                        marker.Delete();
                        client.ResetData("SellGunMarker");
                    }
                    player.Client.StopAnimation();
                    return;
                }
                player.Client.PlayAnimation("amb@world_human_drug_dealer_hard@male@base", "base", (int)(AnimationFlags.Loop));
                client.TriggerEvent("GetForwardVector");
                client.SetData("SellGuns", true);
            }
        }

        [RemoteEvent("GotForwardVector")]
        internal void ForwardVector(Client client, float x, float y, float z)
        {
            var vector3 = new Vector3(x, y, z);
            var sellGunShape = NAPI.ColShape.CreateCylinderColShape(vector3, 1, 1);
            var sellGunMarker = NAPI.Marker.CreateMarker(1, vector3.Subtract(new Vector3(0, 0, 1)), new Vector3(), new Vector3(), 1.2f, new Color(200, 0, 0, 150));
            sellGunShape.SetData("SellGuns", sellGunShape);
            client.SetData("SellGunShape", sellGunShape);
            client.SetData("SellGunMarker", sellGunMarker);
        }

        [Command("drugs")]
        internal void Drugs(Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (player.Client.HasData("Specialization") && player.Team.Specialization == TeamSpecialization.None)
            {
                player.Team.Specialization = TeamSpecialization.Drug_Dealer;
                player.SendNotification("Specialization", "Assigned as Drugs Dealer", "success");
            }
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("SafeHouse"))
            {
                player.Client.SetData("SafeHouse", null);
            }

            if (colShape.HasData("Specialization") && player.Team.TeamEnum == TeamEnum.Criminal)
            {
                player.SendNotification("Specialization", "Select a specialization: /arms or /drugs", "secondary");
                player.Client.SetData("Specialization", null);
            }

            if (colShape.HasData("SellGuns") && !player.Client.HasData("SellGuns"))
            {
                player.SendNotification("Buy Guns", "Buy guns here!", "success");
            }
        }

        internal override void OnPlayerExitColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("SafeHouse") && player.Client.HasData("SafeHouse"))
            {
                player.Client.ResetData("SafeHouse");
            }

            if (colShape.HasData("Specialization") && player.Client.HasData("Specialization"))
            {
                player.Client.ResetData("Specialization");
            }
        }
    }
}
