﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using MainResource.Events;
using System;
using System.Threading.Tasks;
using static MainResource.Models.DatabaseHandler;
using static MainResource.Utility.ChatColors;
using MainResource.Handler;

namespace MainResource.Utility
{
    internal class AircraftShop : ServerEvents
    {

        internal AircraftShop()
        {
            ColShape colShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(-1154.103f, -2715.347f, 19.8873f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(-1154.103f, -2715.347f, 19.8873f - 0.8f), new Vector3(), new Vector3(), 2, new Color(255, 255, 0, 100));
            NAPI.Blip.CreateBlip(575, new Vector3(-1154.103f, -2715.347f, 19.8873f), 0.6f, 2, "Aircraft Shop", 255, 50, true);
            colShape.SetData("eDown:UI", "aircraftShop:Open");
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("eDown:UI") && !player.Client.IsInVehicle)
            {
                player.Client.SetData("eDown:UI", colShape.GetData("eDown:UI"));
            }
        }

        [RemoteEvent("AircraftShopOpen")]
        internal void OnVehicleShopOpen(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            client.Dimension = (uint)client.Value + 1;
        }

        [RemoteEvent("AircraftShopClose")]
        public void OnVehShopClose(Client client, object[] args)
        {
            client.Dimension = 0;
            client.TriggerEvent("UnFreezePlayer");
        }

        [RemoteEvent("AircraftPurchaseAttempt")]
        public void OnVehiclePurchaseAttempt(Client client, object[] args)
        {
            var player = client.GetFromList();
            var hash = (int)args[0];
            var price = (int)args[1];
            var vehicleName = (string)args[2];

            var vehicles = player.PlayerInfo.PlayerVehiclesCount("air");
            if (vehicles == 5)
            {
                client.SendNotification("~r~You cannot have more than 5 aircrafts.");
                return;
            }

            if (player.PlayerInfo.Credits < price)
            {
                client.SendNotification("~r~You don't have that much money with you.");
                return;
            }
            var vehicle = NAPI.Vehicle.CreateVehicle(hash, new Vector3(0, 0, 0), 0, 0, 0);
            vehicle.Dimension = client.Dimension + 1;
            player.PlayerInfo.AddPlayerVehicle(vehicleName, vehicle, hash, player, price);
            player.PlayerInfo.Credits -= Convert.ToUInt32(price);
            UpdateHUD.UpdateCredits(player.Client, player.PlayerInfo.Credits);
            client.SendNotification($"~y~{vehicleName}~w~ has been purchased for $~y~{price}~w~ and added to your garage.");
            vehicle.Delete();
        }
    }
}



