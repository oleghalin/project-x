﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using MainResource.Events;
using System;
using MainResource.Handler;

namespace MainResource.Utility.Vehicles {
    internal class VehicleShop : ServerEvents
    {

        internal VehicleShop()
        {
            ColShape colShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(-31.59031f, -1112.159f, 26.42235f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(-31.59031f, -1112.159f, 26.42235f - 0.8f), new Vector3(), new Vector3(), 2, new Color(255, 255, 0, 100));
            NAPI.Blip.CreateBlip(225, new Vector3(-31.59031f, -1112.159f, 26.42235f), 0.6f, 2, "Vehicle Shop", 255, 50, true);
            colShape.SetData("eDown:UI", "vehicleShop:Open");
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("eDown:UI") && !player.Client.IsInVehicle)
            {
                player.Client.SetData("ColShape", colShape.GetData("eDown:UI"));
            }
        }

        internal override void OnPlayerExitColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("eDown:UI") && player.Client.HasData("ColShape"))
            {
                player.Client.ResetData("ColShape");
                player.Client.TriggerEvent("vehicleShop:Close");
            }
        }

        [RemoteEvent("VehicleShopOpen")]
        internal void OnVehicleShopOpen(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            client.Dimension = (uint)client.Value + 1;
        }

        [RemoteEvent("VehicleShopClose")]
        public void OnVehShopClose(Client client, object[] args)
        {
            client.Dimension = 0;
        }

        [RemoteEvent("VehiclePurchaseAttempt")]
        public void OnVehiclePurchaseAttempt(Client client, object[] args)
        {
            var player = client.GetFromList();
            var hash = (int)args[0];
            var price = (int)args[1];
            var vehicleName = (string)args[2];
            var vehicles = player.PlayerInfo.PlayerVehiclesCount("car");
            if (vehicles == 5)
            {
                client.SendNotification("You cannot have more than 5 cars.");
                return;
            }

            if (player.PlayerInfo.Credits < price)
            {
                client.SendNotification("You don't have that much money with you.");
                return;
            }
            var vehicle = NAPI.Vehicle.CreateVehicle(hash, new Vector3(0, 0, 0), 0, 0, 0);
            vehicle.Dimension = client.Dimension + 1;
            player.PlayerInfo.AddPlayerVehicle(vehicleName, vehicle, hash, player, price);
            player.PlayerInfo.Credits -= Convert.ToUInt32(price);
            UpdateHUD.UpdateCredits(player.Client, player.PlayerInfo.Credits);
            player.SendNotification("Vehicle Shop", $"{vehicleName} has been purchased for {price}$ and added to your garage.", "success");
            vehicle.Delete();
        }
    }
}

        

