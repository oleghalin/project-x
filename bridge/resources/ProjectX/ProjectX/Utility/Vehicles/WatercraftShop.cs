﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using MainResource.Events;
using System;

namespace MainResource.Utility
{
    internal class WatercraftShop : Script
    {

        internal WatercraftShop()
        {
            ColShape colShape = NAPI.ColShape.CreateCylinderColShape(new Vector3(-783.9792f, -1355.469f, 8.999772f), 1, 1);
            NAPI.Marker.CreateMarker(27, new Vector3(-783.9792f, -1355.469f, 8.999772f - 0.8f), new Vector3(), new Vector3(), 2, new Color(255, 255, 0, 100));
            NAPI.Blip.CreateBlip(404, new Vector3(-783.9792f, -1355.469f, 8.999772f), 0.6f, 2, "Watercraft Shop", 255, 50, true);
            colShape.SetData("WatercraftShop", "watercraftShop:Open");
        }

        [ServerEvent(Event.PlayerEnterColshape)]
        internal void EnterVehicleShop(ColShape colShape, Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            if (colShape.HasData("WatercraftShop") && !client.IsInVehicle)
            {
                client.SetData("eDown:UI", colShape.GetData("WatercraftShop"));
            }
        }

        [RemoteEvent("WatercraftShopOpen")]
        internal void OnVehicleShopOpen(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            client.Dimension = (uint)client.Value + 1;
        }

        [ServerEvent(Event.PlayerExitColshape)]
        internal void ExitVehicleShop(ColShape colShape, Client client)
        {
            if (!client.GetPlayer(out var player)) return;
            client.ResetData("ColShape");
            client.TriggerEvent("watercraftShop:Close");
        }

        [RemoteEvent("WatercraftShopClose")]
        public void OnVehShopClose(Client client, object[] args)
        {
            client.Dimension = 0;
            NAPI.ClientEvent.TriggerClientEvent(client, "UnFreezePlayer");
        }

        [RemoteEvent("WatercraftPurchaseAttempt")]
        public void OnVehiclePurchaseAttempt(Client client, object[] args)
        {
            var player = client.GetFromList();
            var hash = (int)args[0];
            var price = (int)args[1];
            var vehicleName = (string)args[2];

            var vehicles = player.PlayerInfo.PlayerVehiclesCount("water");
            if (vehicles == 5)
            {
                client.SendNotification("~r~You cannot have more than 5 boats.");
                return;
            }

            if (player.PlayerInfo.Credits < price)
            {
                client.SendNotification("~r~You don't have that much money with you.");
                return;
            }
            var vehicle = NAPI.Vehicle.CreateVehicle(hash, new Vector3(0, 0, 0), 0, 0, 0);
            vehicle.Dimension = client.Dimension + 1;
            player.PlayerInfo.AddPlayerVehicle(vehicleName, vehicle, hash, player, price);
            player.PlayerInfo.Credits -= Convert.ToUInt32(price);
            UpdateHUD.UpdateCredits(player.Client, player.PlayerInfo.Credits);
            client.SendNotification($"~y~{vehicleName}~w~ has been purchased for $~g~{price}~w~ and added to your garage.");
            vehicle.Delete();
        }
    }
}



