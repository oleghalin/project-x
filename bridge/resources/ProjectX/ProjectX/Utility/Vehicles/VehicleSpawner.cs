﻿using GTANetworkAPI;
using MainResource.Models;
using MainResource.Models.PlayerData;
using Newtonsoft.Json;
using System;
using static MainResource.Utility.Config;
using MainResource.Handler;
using System.Linq;

namespace MainResource.Utility {
    internal class PlayerVehicleSpawner {
        internal Vector3 ColShapeVector { get; private set; }
        internal Vector3 CarSpawnVector { get; private set; }
        internal float Rotation { get; private set; }
        internal string Class { get; private set; }

        internal PlayerVehicleSpawner(string clas, Vector3 colShapeVector, Vector3 carSpawnVector, float rotation) {
            Class = clas;
            Rotation = rotation;
            ColShapeVector = colShapeVector;
            CarSpawnVector = carSpawnVector;
            ColShape colShape = NAPI.ColShape.CreateCylinderColShape(colShapeVector, 1, 1);
            NAPI.Marker.CreateMarker(27, colShapeVector.Subtract(new Vector3(0,0,0.8f)), new Vector3(), new Vector3(), 2, new Color(0, 200, 0, 100));
            if (Class == "Car")
            {
                NAPI.Blip.CreateBlip(225, colShapeVector, 0.5f, 3, "Vehicle Spawn", 255, 50, true);
            }
            else if (Class == "Water")
            {
                NAPI.Blip.CreateBlip(404, colShapeVector, 0.5f, 3, "Boat Spawn", 255, 50, true);
            }
            else if (Class == "Air")
            {
                NAPI.Blip.CreateBlip(575, colShapeVector, 0.5f, 3, "Aircraft Spawn", 255, 50, true);
            }
            
            colShape.SetData("VehicleSpawner", this);
            colShape.SetData("Spawner", "vehicles:Open");
        }
    }

    internal class CarSpawner : ServerEvents
    {

        internal override void OnResourceStart()
        {
            new PlayerVehicleSpawner("Car",new Vector3(91.63044f, 3704.556f, 39.55053f), new Vector3(97.52669f, 3710.696f, 39.66465f), 183.0463f);
            new PlayerVehicleSpawner("Car",new Vector3(411.003f, -974.5377f, 29.42315f), new Vector3(408.2616f, -984.675f, 29.26618f), 52.1656f);
            new PlayerVehicleSpawner("Car",new Vector3(811.7014f, -1604.407f, 31.79948f), new Vector3(806.8544f, -1622.006f, 31.19076f), 66.55665f);
            new PlayerVehicleSpawner("Car",new Vector3(-354.9614f, -125.3956f, 39.43062f), new Vector3(-382.1208f, -137.3373f, 38.68518f), 297.5398f);
            new PlayerVehicleSpawner("Car",new Vector3(1817.405f, 3660.349f, 34.27647f), new Vector3(1825.008f, 3659.474f, 33.94494f), 210.2345f);
            new PlayerVehicleSpawner("Car",new Vector3(732.3328f, 2535.573f, 73.19604f), new Vector3(737.1102f, 2527.548f, 73.2275f), 267.9224f);
            new PlayerVehicleSpawner("Car",new Vector3(-1684.935f, 57.64395f, 64.04377f), new Vector3(-1694.165f, 57.77829f, 64.68445f), 161.0002f);
            new PlayerVehicleSpawner("Car",new Vector3(-561.7755f, -1009.744f, 22.17812f), new Vector3(-560.6597f, -1017.844f, 22.17812f), 91.98866f);
            new PlayerVehicleSpawner("Car",new Vector3(-759.8115f, -584.8715f, 30.27626f), new Vector3(-771.1969f, -592.2025f, 30.12628f), 357.7043f);
            new PlayerVehicleSpawner("Car",new Vector3(-43.21352f, -1108.724f, 26.43758f), new Vector3(-47.66395f, -1116.444f, 26.43425f), 358.6129f);
            new PlayerVehicleSpawner("Car",new Vector3(431.3682f, -1508.657f, 29.29162f), new Vector3(439.8222f, -1517.183f, 29.27918f), 141.2081f);
            new PlayerVehicleSpawner("Car",new Vector3(300.2322f, -575.7875f, 43.26084f), new Vector3(295.7832f, -583.7145f, 43.15127f), 344.2067f);
            new PlayerVehicleSpawner("Car",new Vector3(-3154.528f, 1098.998f, 20.85455f), new Vector3(-3152.422f, 1092.496f, 20.70613f), 276.7943f);
            new PlayerVehicleSpawner("Car",new Vector3(-3012.211f, 81.42843f, 11.67635f), new Vector3(-3000.824f, 79.6881f, 11.60851f), 327.5638f);
            new PlayerVehicleSpawner("Car",new Vector3(-2181.887f, -396.9384f, 13.36658f), new Vector3(-2167.681f, -398.0511f, 13.37586f), 83.14462f);
            new PlayerVehicleSpawner("Car",new Vector3(-1605.932f, -1027.958f, 13.07925f), new Vector3(-1585.4f, -1031.133f, 13.01865f), 22.85311f);
            new PlayerVehicleSpawner("Car",new Vector3(-1275.002f, -1165.061f, 5.82294f), new Vector3(-1273.133f, -1158.502f, 6.18881f), 116.8056f);
            new PlayerVehicleSpawner("Car",new Vector3(-1185.955f, -1507.462f, 4.379669f), new Vector3(-1190.823f, -1504.104f, 4.369524f), 303.1068f);
            new PlayerVehicleSpawner("Car",new Vector3(-216.3495f, -1501.872f, 31.44653f), new Vector3(-219.3916f, -1491.731f, 31.26488f), 317.7656f);
            new PlayerVehicleSpawner("Car",new Vector3(42.46868f, -1597.949f, 29.59777f), new Vector3(40.64312f, -1591.999f, 29.46017f), 50.6348f);
            new PlayerVehicleSpawner("Car",new Vector3(310.017f, -2033.372f, 20.66927f), new Vector3(313.4282f, -2028.992f, 20.53713f), 318.4753f);
            new PlayerVehicleSpawner("Car",new Vector3(246.7981f, -2561.481f, 5.736663f), new Vector3(256.3819f, -2570.82f, 5.701787f), 23.1071f);
            new PlayerVehicleSpawner("Car",new Vector3(502.1476f, -3052.16f, 6.169291f), new Vector3(510.0541f, -3053.552f, 6.069631f), 2.518499f);
            new PlayerVehicleSpawner("Car",new Vector3(1425.962f, -2584.742f, 47.99137f), new Vector3(1429.035f, -2592.108f, 48.03508f), 161.882f);
            new PlayerVehicleSpawner("Car",new Vector3(840.7172f, -2120.736f, 29.85722f), new Vector3(832.496f, -2119.155f, 29.40251f), 137.006f);
            new PlayerVehicleSpawner("Car",new Vector3(1376.839f, -2079.467f, 51.99856f), new Vector3(1381.359f, -2075.478f, 51.99856f), 38.93892f);
            new PlayerVehicleSpawner("Car",new Vector3(1136.86f, -482.4185f, 65.94437f), new Vector3(1130.476f, -485.3995f, 65.61486f), 255.826f);
            new PlayerVehicleSpawner("Car",new Vector3(886.2849f, -0.8374839f, 78.76497f), new Vector3(869.8922f, -11.85769f, 78.76411f), 239.0409f);
            new PlayerVehicleSpawner("Car",new Vector3(599.5362f, 90.06458f, 92.81765f), new Vector3(598.5472f, 98.26541f, 92.9063f), 246.8448f);
            new PlayerVehicleSpawner("Car",new Vector3(219.8818f, 372.1646f, 106.3161f), new Vector3(209.342f, 375.5936f, 107.0171f), 347.2097f);
            new PlayerVehicleSpawner("Car",new Vector3(-65.90171f, 885.3155f, 235.8035f), new Vector3(-67.49699f, 894.9006f, 235.5414f), 115.7991f);
            new PlayerVehicleSpawner("Car",new Vector3(1695.693f, 4793.343f, 41.92137f), new Vector3(1690.6f, 4788.09f, 41.92151f), 85.85966f);
            new PlayerVehicleSpawner("Car",new Vector3(1737.17f, 6406.504f, 34.96054f), new Vector3(1729.104f, 6404.145f, 34.5234f), 334.3483f);
            new PlayerVehicleSpawner("Car",new Vector3(175.6667f, 6634.95f, 31.67356f), new Vector3(181.2299f, 6632.567f, 31.57421f), 176.7926f);
            new PlayerVehicleSpawner("Car",new Vector3(-777.3215f, 5539.032f, 33.4945f), new Vector3(-762.9156f, 5548.023f, 33.48608f), 179.601f);
            new PlayerVehicleSpawner("Car",new Vector3(-1492.041f, 4979.596f, 63.37476f), new Vector3(-1490.978f, 4975.033f, 63.7088f), 43.53952f);
            new PlayerVehicleSpawner("Car",new Vector3(-2204.204f, 4274.544f, 48.28375f), new Vector3(-2197.417f, 4268.994f, 48.51091f), 147.4321f);
            new PlayerVehicleSpawner("Car",new Vector3(-1924.094f, 2061.46f, 140.8349f), new Vector3(-1918.844f, 2056.935f, 140.7357f), 255.4053f);
            new PlayerVehicleSpawner("Car",new Vector3(-896.7292f, -2583.494f, 13.98091f), new Vector3(-896.2651f, -2592.786f, 13.83086f), 326.8793f);
            new PlayerVehicleSpawner("Car",new Vector3(-255.2186f, 6339.982f, 32.42618f), new Vector3(-261.8576f, 6344.111f, 32.42635f), 272.3163f);
            new PlayerVehicleSpawner("Car",new Vector3(-485.2395f, -348.846f, 34.50204f), new Vector3(-491.117f, -344.1021f, 34.37094f), 260.4504f);
            new PlayerVehicleSpawner("Car",new Vector3(1411.678f, 3604.436f, 34.99076f), new Vector3(1413.349f, 3600.63f, 34.88295f), 112.1269f);
            new PlayerVehicleSpawner("Car",new Vector3(1037.919f, -765.5052f, 57.97108f), new Vector3(1045.87f, -774.5164f, 58.01694f), 93.00458f);
            new PlayerVehicleSpawner("Car",new Vector3(-195.6046f, 316.9568f, 96.94559f), new Vector3(-205.6272f, 314.733f, 96.9457f), 183.6317f);
            new PlayerVehicleSpawner("Car",new Vector3(-2317.628f, 273.5063f, 169.602f), new Vector3(-2322.325f, 278.2068f, 169.4672f), 25.14986f);
            new PlayerVehicleSpawner("Car",new Vector3(2556.978f, 2608.885f, 38.07611f), new Vector3(2550.469f, 2614.828f, 37.94484f), 20.47318f);
            new PlayerVehicleSpawner("Car",new Vector3(2680.531f, 1664.124f, 24.59811f), new Vector3(2673.046f, 1660.112f, 24.48861f), 91.88665f);
            new PlayerVehicleSpawner("Car",new Vector3(2576.234f, 305.4462f, 108.6086f), new Vector3(2574.886f, 313.5594f, 108.4583f), 0.356355f);
            new PlayerVehicleSpawner("Car",new Vector3(1731.567f, -1643.483f, 112.5823f), new Vector3(1725.829f, -1635.893f, 112.5072f), 190.1352f);
            new PlayerVehicleSpawner("Car",new Vector3(-273.4873f, -2085.601f, 27.75542f), new Vector3(-265.905f, -2085.275f, 27.62041f), 292.0448f);
            new PlayerVehicleSpawner("Car",new Vector3(-426.8316f, 1200.43f, 325.7583f), new Vector3(-420.1834f, 1202.447f, 325.6417f), 233.3474f);
            new PlayerVehicleSpawner("Car",new Vector3(96.30636f, -148.9041f, 54.8982f), new Vector3(104.2419f, -145.6058f, 54.74871f), 70.83781f);
            new PlayerVehicleSpawner("Air", new Vector3(-1688.455f, -3147.296f, 24.3224f), new Vector3(-1651.093f, -3142.406f, 13.99206f), 328.2678f);
            new PlayerVehicleSpawner("Air", new Vector3(1744.946f, 3300.687f, 41.2235f), new Vector3(1732.32f, 3307.207f, 41.22346f), 194.0085f);
            new PlayerVehicleSpawner("Water", new Vector3(-799.8772f, -1418.178f, 1.595f), new Vector3(-798.55f, -1411.985f, 0.1206684f), 230.0377f);
            new PlayerVehicleSpawner("Water", new Vector3(554.6638f, -3180.551f, 6.069474f), new Vector3(570.2308f, -3162.178f, 1.542377f), 187.7355f);
            new PlayerVehicleSpawner("Water", new Vector3(1737.28f, 3973.545f, 31.9787f), new Vector3(1721.845f, 3985.002f, 29.87451f), 77.03335f);
        }

        [RemoteEvent("VehiclesOpen")]
        internal void OnVehiclesOpen(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            try
            {
                if (player.ColShape != null)
                {
                    var spawner = (PlayerVehicleSpawner)player.ColShape.GetData("VehicleSpawner");
                    client.SetData("SpawnerColShape", spawner);
                    GetSpawnerType(player, player.ColShape);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [RemoteEvent("vehicles:sell")]
        internal void SellVehicle(Client client, string guid)
        {
            if (!client.GetPlayer(out var player)) return;
            if (client.HasData("SpawnerColShape"))
            {
                var objGuid = JsonConvert.DeserializeObject(guid);
                var guidd = Guid.Parse(objGuid.ToString());
                player.PlayerInfo.SellVehicle(player, guidd);
                if (player.ColShape != null)
                {
                    GetSpawnerType(player, player.ColShape);
                }
            }
        }

        [RemoteEvent("vehicles:selected")] 
        internal void VehicleSelection(Client client, string guid)
        {
            if (!client.GetPlayer(out var player)) return;
            if (client.IsInVehicle) {
                client.SendNotification("~r~Get out of your vehicle.");
                return;
            }
            if (!client.HasData("SpawnerColShape")) return;
            var spawner = (PlayerVehicleSpawner)client.GetData("SpawnerColShape");
            var objGuid = JsonConvert.DeserializeObject(guid);
            var guidd = Guid.Parse(objGuid.ToString());
            var playerVeh = player.VehicleInfo.Vehicle;
            var cars = NAPI.Pools.GetAllVehicles();
            var rotation = spawner.Rotation;
            var spawnLocation = spawner.CarSpawnVector;

            foreach(Vehicle vehicle1 in cars)
            {
                if (vehicle1.Position.DistanceTo2D(spawnLocation) < 2f && player.VehicleInfo.Vehicle != vehicle1)
                {
                    client.SendNotification($"~r~Another vehicle is blocking the spawn.");
                    return;
                }
            }
            if (player.VehicleInfo.Inventory.Count != 0) player.VehicleInfo.Inventory.Clear();
            if (playerVeh == null)
            {
                player.PlayerInfo.SpawnPlayerVehicle(guidd, player, spawnLocation, rotation);
                client.TriggerEvent("vehicles:Close");
                return;
            }

            client.TriggerEvent("vehicles:Close");
            player.PlayerInfo.UpdatePlayerVehicle(player.VehicleInfo.VehicleID, player.VehicleInfo.Vehicle);
            player.PlayerInfo.SpawnPlayerVehicle(guidd, player, spawnLocation, rotation);
            playerVeh.Delete();
        }

        internal void GetSpawnerType(Player player, ColShape colShape)
        {
            try
            {
                if (colShape.HasData("VehicleSpawner"))
                {
                    var spawner = (PlayerVehicleSpawner)colShape.GetData("VehicleSpawner");
                    switch (spawner.Class)
                    {
                        case "Car":
                            var vehs = player.PlayerInfo.Vehicles.Values.Where(a => (a.Class != 14 && a.Class != 15 && a.Class != 16)).ToList();
                            string cars = JsonConvert.SerializeObject(vehs);
                            player.Client.TriggerEvent("vehicles:setPlayerVehicles", cars);
                            break;
                        case "Air":
                            var aircraft = player.PlayerInfo.Vehicles.Values.Where(a => (a.Class == 15 || a.Class == 16)).ToList();
                            string air = JsonConvert.SerializeObject(aircraft);
                            player.Client.TriggerEvent("vehicles:setPlayerVehicles", air);
                            break;
                        case "Water":
                            var watercraft = player.PlayerInfo.Vehicles.Values.Where(a => a.Class == 14).ToList();
                            string water = JsonConvert.SerializeObject(watercraft);
                            player.Client.TriggerEvent("vehicles:setPlayerVehicles", water);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("Spawner") && !player.Client.IsInVehicle)
            {
                player.Client.SetData("eDown:UI", colShape.GetData("Spawner"));
            }
        }
    }
}
