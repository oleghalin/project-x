﻿using GTANetworkAPI;
using MainResource.Handler;
using MainResource.Models.PlayerData;
using MainResource.Classes;

namespace MainResource.Utility
{
    class ClothingShop : ServerEvents
    {
        [RemoteEvent("BuyClothes")]
        internal void BuyClothes(Client client, int id, int draw, int tex)
        {
            if (!client.GetPlayer(out var player)) return;
            player.PlayerInfo.Clothes.Add(new Clothes(id, draw, tex));
            player.PlayerInfo.SetPlayerClothes(player);
            player.SendNotification("Clothing Shop", "You have purchased some clothing", "success");
        }
        
        [RemoteEvent("ClothingClose")]
        internal void SetClothes(Client client)
        {
            if (!client.GetPlayer(out var player)) return;

            player.PlayerInfo.SetPlayerClothes(player);
            player.PlayerInfo.SavePlayerClothes(player);
        }
    }
}
