﻿using MainResource.Models;
using System;
using System.Threading;
using MainResource.MongoDB;

namespace MainResource.Utility
{
    public class Config
    {
        internal static DriverContainer Database { get; set; } = new DriverContainer();
        
        internal static void SetupDatabase()
        {
            if (!Database.LoadDatabase())
            {
                Console.WriteLine("Error loading Database");
                Thread.Sleep(5000);
                Environment.Exit(-1);
                return;
            }
            
            Database.InitializeCollectionHolders();
            Console.WriteLine("Loaded Database.");
        }
    }
}