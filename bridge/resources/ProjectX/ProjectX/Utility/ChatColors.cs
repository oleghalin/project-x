﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MainResource.Utility
{
    static class ChatColors
    {
        public static string White = "#ffffff";
        public static string Red = "#ff0000";
        public static string Yellow = "#ffff00";
        public static string Blue = "#0000ff";
        public static string Green = "#33cc33";
        public static string Orange = "#ff6600";
        public static string Purple = "#ce42f5";
    }
}
