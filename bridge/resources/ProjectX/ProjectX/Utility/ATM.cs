﻿using GTANetworkAPI;
using MainResource.Models.PlayerData;
using System;
using System.Linq;
using MainResource.Handler;
using System.Collections.Generic;

namespace MainResource.Utility {
	internal class Atm : ServerEvents
    {
        private readonly List<Vector3> _atmCoords = new List<Vector3>()
        {
			new Vector3(-1109.797f, -1690.808f, 4.375014f),
			new Vector3(-821.6062f, -1081.885f, 11.13243f),
			new Vector3(-537.8409f, -854.5145f, 29.28953f),
			new Vector3(-1315.744f, -834.6907f, 16.96173f),
			new Vector3(-1570.069f, -546.6727f, 34.95547f),
			new Vector3(-866.6416f, -187.8008f, 37.84286f),
			new Vector3(-721.1284f, -415.5296f, 34.98175f),
			new Vector3(-254.3758f, -692.4947f, 33.63751f),
			new Vector3(24.37422f, -946.0142f, 29.35756f),
			new Vector3(130.1186f, -1292.669f, 29.26953f),
			new Vector3(288.8256f, -1282.364f, 29.64128f),
			new Vector3(1077.768f, -776.4548f, 58.23997f),
			new Vector3(-1205.024f, -326.2916f, 37.83985f),
			new Vector3(-57.64693f, -92.66162f, 57.77995f),
			new Vector3(527.3583f, -160.6381f, 57.0933f),
			new Vector3(-165.1658f, 234.8314f, 94.92194f),
			new Vector3(-2072.445f, -317.3048f, 13.31597f),
			new Vector3(-3241.082f, 997.5428f, 12.55044f),
			new Vector3(-1091.462f, 2708.637f, 18.95291f),
			new Vector3(1822.637f, 3683.131f, 34.27678f),
			new Vector3(1686.753f, 4815.806f, 42.00874f),
			new Vector3(1701.209f, 6426.569f, 32.76408f),
			new Vector3(-97.23336f, 6455.469f, 31.46682f),
			new Vector3(-386.7451f, 6046.102f, 31.50172f),
			new Vector3(-660.703f, -853.971f, 24.484f),
			new Vector3(-2295.377f, 358.241f, 174.648f),
			new Vector3(-1409.782f, -100.41f, 52.387f),
			new Vector3(285.4945f, 143.5294f, 104.173f),
			new Vector3(-203.825f, -861.4416f, 30.26764f),
			new Vector3(-526.4661f, -1222.861f, 18.45497f),
			new Vector3(155.5418f, 6642.638f, 31.60913f),
            new Vector3(-3144.225f, 1127.509f, 20.85532f),
            new Vector3(-133.3687f, 6366.062f, 31.47505f),
            new Vector3(150.051f, -1040.256f, 29.37408f),
            new Vector3(1139.102f, -469.1525f, 66.73061f),
            new Vector3(314.3955f, -278.5664f, 54.17077f),
            new Vector3(2559.309f, 350.9567f, 108.6215f),
            new Vector3(-30.41421f, -724.3189f, 44.23418f),
            new Vector3(-261.5515f, -2012.597f, 30.14558f),
            new Vector3(-2963.046f, 482.9453f, 15.7031f),
            new Vector3(1175.017f, 2706.512f, 38.09404f)
        };

        internal override void OnResourceStart()
        {
            foreach (var vector3 in _atmCoords)
            {
                var colShape = NAPI.ColShape.CreateCylinderColShape(vector3, 1, 1);
                NAPI.Marker.CreateMarker(1, vector3.Subtract(new Vector3(0, 0, 1)), new Vector3(), new Vector3(), 0.5f, new Color(0, 200, 0, 100));
                NAPI.Blip.CreateBlip(207, vector3, 0.6f, 52, "ATM", 255, 50, true);
                colShape.SetData("eDown:UI", "bank:Open");
            }
        }

        internal override void OnPlayerEnterColShape(ColShape colShape, Player player)
        {
            if (colShape.HasData("eDown:UI") && !player.Client.IsInVehicle)
            {
                player.Client.SetData("eDown:UI", colShape.GetData("eDown:UI"));
            }
        }

        [RemoteEvent("BankOpen")]
        internal void OnBankOpen(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;

            client.TriggerEvent("bank:setBalance", player.PlayerInfo.Balance, player.PlayerInfo.PlayerName);
        }

        [RemoteEvent("deposit")]
        public void OnBankDeposit(Client client, object[] args) {
            if (!client.GetPlayer(out var player)) return;
            var deposit = Convert.ToUInt32(args[0]);

            if (player.PlayerInfo.Credits < deposit)
            {
                client.SendNotification("You don't have that much money to deposit!");
                return;
            }
            player.TakeCredits(deposit);
            player.AddBalance(deposit);
            client.SendNotification($"~g~${deposit}~w~ has been added into your account.");
            client.TriggerEvent("bank:setBalance", player.PlayerInfo.Balance, client.Name);
        }

        [RemoteEvent("withdraw")]
        public void OnBankWithdraw(Client client, object[] args) {
            if (!client.GetPlayer(out var player)) return;

            var withdraw = Convert.ToUInt32(args[0]);
            if (player.PlayerInfo.Balance < withdraw)
            {
                client.SendNotification("You don't have that much money to withdraw!");
                return;
            }
            player.TakeBalance(withdraw);
            player.AddCredits(withdraw);
            client.SendNotification($"~g~${withdraw}~w~ has been taken from your account.");
            client.TriggerEvent("bank:setBalance", player.PlayerInfo.Balance, client.Name);
        }

        [RemoteEvent("send")]
        internal void OnBankSend(Client client, object[] args)
        {
            if (!client.GetPlayer(out var player)) return;
            var clientName = (string)args[1];
            var amount = Convert.ToUInt32(args[0]);
            var reciever = NAPI.Pools.GetAllPlayers().SingleOrDefault(a => a.Name == clientName);
            

            if (reciever == null)
            {
                player.Client.SendNotification($"~y~{clientName.ToUpper()}~w~ is not online.");
                return;
            }

            if (reciever == player.Client)
            {
                player.Client.SendNotification("You cannot send money to yourself!");
                return;
            }

            if (amount > player.PlayerInfo.Balance)
            {
                client.SendNotification("You don't have enough money in your account to send.");
                return;
            }

            var recieverPlayer = reciever.GetFromList();
            recieverPlayer.AddBalance(amount);
            player.TakeBalance(amount);
            client.TriggerEvent("bank:setBalance", player.PlayerInfo.Balance, client.Name);
            player.Client.SendNotification($"You sent $~g~{amount}~w~ to ~y~{clientName.ToUpper()}~w~.");
        }
    }
}