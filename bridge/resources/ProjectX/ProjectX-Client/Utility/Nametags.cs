﻿using RAGE;
using RAGE.Elements;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace ProjectX_Client.Utility
{
    class Nametags : Events.Script
    {
        int maxDistance = 25 * 25;
        double width = 0.03;
        double height = 0.0065;
        double border = 0.001;
        int screenX;
        int screenY;
        double nametagX;
        double nametagY;

        internal Nametags()
        {
            Events.Tick += Tick;
        }

        public void Tick(List<Events.TickNametagData> nametags)
        {
            if (nametags == null) return;
            foreach (var nametag in nametags)
            {
                if (nametag.Distance < maxDistance)
                {
                    var scale = (nametag.Distance / maxDistance);
                    if (scale < 0.6f) scale = 0.6f;

                    var health = nametag.Player.GetHealth();
                    health = health < 100 ? 0 : ((health - 100) / 100);
                    var armour = nametag.Player.GetArmour() / 100;
                    RAGE.Game.Graphics.GetScreenResolution(ref screenX, ref screenY);
                    nametagY -= scale * (0.005 * (screenY / 1080));
                    RAGE.Game.UIText.Draw(nametag.Player.Name, new Point(Convert.ToInt32(nametag.ScreenX), Convert.ToInt32(nametag.ScreenY)), 0.4f, Color.FromArgb(255, 255, 255), RAGE.Game.Font.ChaletComprimeCologne, true);
                    

                    if (RAGE.Game.Player.IsPlayerFreeAimingAtEntity(nametag.Player.Handle))
                    {
                        var y2 = Convert.ToSingle(nametag.ScreenY + 0.042);

                        if (armour > 0)
                        {
                            var x2 = Convert.ToSingle(nametag.ScreenY - width / 2 - border / 2);

                            RAGE.Game.Graphics.DrawRect(x2, y2, Convert.ToSingle(width + border * 2), 0.0085f, 0, 0, 0, 200, 0);
                            RAGE.Game.Graphics.DrawRect(x2, y2, Convert.ToSingle(width), Convert.ToSingle(height), 150, 150, 150, 255, 0);
                            RAGE.Game.Graphics.DrawRect(Convert.ToSingle(x2 - width / 2 * (1 - health)), y2, Convert.ToSingle(width * health), Convert.ToSingle(height), 255, 255, 255, 200, 0);

                            x2 = Convert.ToSingle(nametag.ScreenX + width / 2 + border / 2);

                            RAGE.Game.Graphics.DrawRect(x2, y2, Convert.ToSingle(width + border * 2), Convert.ToSingle(height + border * 2), 0, 0, 0, 200, 0);
                            RAGE.Game.Graphics.DrawRect(x2, y2, Convert.ToSingle(width), Convert.ToSingle(height), 41, 66, 78, 255, 0);
                            RAGE.Game.Graphics.DrawRect(Convert.ToSingle(x2 - width / 2 * (1 - armour)), y2, Convert.ToSingle(width * armour), Convert.ToSingle(height), 48, 108, 135, 200, 0);
                        }
                        else
                        {
                            RAGE.Game.Graphics.DrawRect(nametag.ScreenX, y2, Convert.ToSingle(width + border * 2), Convert.ToSingle(height + border * 2), 0, 0, 0, 200, 0);
                            RAGE.Game.Graphics.DrawRect(nametag.ScreenX, y2, Convert.ToSingle(width), Convert.ToSingle(height), 150, 150, 150, 255, 0);
                            RAGE.Game.Graphics.DrawRect(Convert.ToSingle(nametag.ScreenX - width / 2 * (1 - health)), y2, Convert.ToSingle(width * health), Convert.ToSingle(height), 255, 255, 255, 200, 0);
                        }
                    }
                }
            }
        }
    }
}
