﻿using RAGE;
using RAGE.Elements;
using System;
using System.Collections.Generic;

namespace Main.CEF
{
    internal class TruckerJobSpawns
    {
        internal Vector3 colShapeVector;
        internal Marker marker;
        internal Blip blip;
        static List<TruckerJobSpawns> truckerJobs = new List<TruckerJobSpawns>();

        internal TruckerJobSpawns(Vector3 colShapeVector)
        {
            this.colShapeVector = colShapeVector;
            marker = new Marker(1, colShapeVector.Subtract(new Vector3(0, 0, 1)), 1, new Vector3(), new Vector3(), new RGBA(255, 200, 0, 100));
            blip = new Blip(479, colShapeVector, "Trucker", 0.8f, 0, 255, 50, true);
            truckerJobs.Add(this);
        }

        internal static void DeleteTruckerJobSpawns()
        {
            if (truckerJobs.Count != 0)
            {
                foreach (TruckerJobSpawns truckerJobSpawns in truckerJobs)
                {
                    truckerJobSpawns.blip.Destroy();
                    truckerJobSpawns.marker.Destroy();
                }
            }
        }
    }

    public class TruckerWindow : HtmlWindow
    {
        public TruckerWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "Trucker";
            WindowPath = "package://ServerUI/index.html#/Trucker";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        Marker Marker { get; set; } = null;
        Blip Blip { get; set; } = null;
        int tick { get; set; } = 0;
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("trucker:Close", (object[] args) => 
            {
                HideWindow();
            });
            Events.Add("trucker:Open", (object[] args) => 
            {
                RenderWindow();
            });
            Events.Add("trucker:Spawn", (object[] args) =>
            {
                new TruckerJobSpawns(new Vector3(803.7892f, -1666.946f, 30.86441f));
                new TruckerJobSpawns(new Vector3(541.2504f, -1934.19f, 24.5028f));
            });
            Events.Add("trucker:Selection", (object[] args) =>
            {
                float x = (float)args[0], y = (float)args[1], z = (float)args[2];
                var vector = new Vector3(x, y, z);
                Events.CallRemote("TruckerSelectJob", x,y,z, args[3], args[4]);
                Marker = new Marker(1, vector.Subtract(new Vector3(0,0,1)), 2, new Vector3(), new Vector3(), new RGBA(200, 200, 0, 100));
                Blip = new Blip(1, vector, "Trucker Destination", 0.8f, 5, 255);
                Blip.SetRoute(true);
                Blip.SetRouteColour(5);
                Blip.SetFlashTimer(5000);
                Events.Tick += OnUpdate;
                TruckerJobSpawns.DeleteTruckerJobSpawns();
            });
            Events.Add("trucker:Complete", (object[] args) => 
            {
                Blip.SetRoute(false);
                Marker.Destroy();
                Blip.Destroy();
                Events.Tick -= OnUpdate;
                tick = 0;
            });
            Events.Add("DeleteJobs", (object[] args) =>
            {
                TruckerJobSpawns.DeleteTruckerJobSpawns();
            });
        }

        internal void OnUpdate(List<Events.TickNametagData> nametags)
        {
            tick++;

            if (tick == 60)
            {
                tick = 0;
                Events.CallRemote("IsNearbyTrailer");
            }
        }
    }
}
