﻿using RAGE;
using RAGE.Elements;
using System;

namespace Main.CEF
{
    public class ClothingShop : HtmlWindow
    {
        int ID { get; set; } = 0;
        int Drawable { get; set; } = 0;
        int Texture { get; set; } = 0;
        public ClothingShop()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "ClothingShop";
            WindowPath = "package://ServerUI/index.html#/ClothingShop";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("clothingShop:Close", (object[] args) => {
                HideWindow();
                //Player.LocalPlayer.SetDefaultComponentVariation();
                Events.CallRemote("ClothingClose");
            });
            Events.Add("clothingShop:Open", (object[] args) => {
                RenderWindow();
                CefWindow.ExecuteJs($"EventBus.$emit('clothingShop:GetGender', 1)");
            });
            Events.Add("clothingShop:TextureSelect", (object[] args) =>
            {
                var id = args[0];
                var drawable = args[1];
                var texture = args[2];
                ID = Convert.ToInt32(id);
                Drawable = Convert.ToInt32(drawable);
                Texture = Convert.ToInt32(texture);
                //Player.LocalPlayer.SetComponentVariation(ID, Drawable, Texture, 0);
                Events.CallRemote("BuyClothes", ID, Drawable, Texture);
            });
            Events.Add("clothingShop:Select", (object[] args) =>
            {
                var id = args[0];
                var drawable = args[1];
                ID = Convert.ToInt32(id);
                Drawable = Convert.ToInt32(drawable);
                Events.CallRemote("BuyClothes", ID, Drawable, Texture);
                //Player.LocalPlayer.SetComponentVariation(ID, Drawable, 0, 0);
                var textures = Player.LocalPlayer.GetNumberOfTextureVariations(ID, Drawable);
                if (textures != 0) textures -= 1;
                CefWindow.ExecuteJs($"EventBus.$emit('clothingShop:GetMaxTextures', {textures})");
            });
            Events.Add("clothingShop:Buy", (object[] args) =>
            {
                Events.CallRemote("BuyClothes", ID, Drawable, Texture);
            });
        }
    }
}
