﻿using RAGE;
using RAGE.Elements;
using System;

namespace Main.CEF
{
    public class WatercraftShopWindow : HtmlWindow
    {
        public WatercraftShopWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "WatercraftShop";
            WindowPath = "package://ServerUI/index.html#/WatercraftShop";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        int shopCam;
        Vehicle vehicle { get; set; } = null;
        float initRota = 65f;
        int price;
        string name;
        int hash;
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("watercraftShop:Close", (object[] args) => {
                initRota = 65f;
                if (vehicle != null) vehicle.Destroy();
                vehicle = null;
                Events.CallRemote("WatercraftShopClose");
                HideWindow();
                RAGE.Game.Cam.RenderScriptCams(false, true, 1000, false, false, 0);
                RAGE.Game.Cam.DestroyCam(shopCam, true);
            });
            Events.Add("watercraftShop:Open", (object[] args) => {
                RenderWindow();
                Events.CallRemote("WatercraftShopOpen");
                Player.LocalPlayer.FreezePosition(true);
                shopCam = RAGE.Game.Cam.CreateCameraWithParams(RAGE.Game.Misc.GetHashKey("DEFAULT_SCRIPTED_CAMERA"), -764.5856f, -1351.435f, 8f, 0, 0, 237.3279f, 70, false, 2);
                RAGE.Game.Cam.SetCamActive(shopCam, true);
                RAGE.Game.Cam.RenderScriptCams(true, true, 500, false, false, 0);
            });
            Events.Add("watercraftShop:selection", (object[] args) => {
                initRota = 65f;
                hash = (int)args[0];
                price = (int)args[1];
                name = (string)args[2];

                if (vehicle != null)
                {
                    vehicle.Destroy();
                }
                vehicle = new Vehicle((uint)hash, new Vector3(-735.2311f, -1363.533f, 0.4748882f));
                vehicle.SetHeading(initRota);
            });
            Events.Add("watercraftShop:purchase", (object[] args) => {
                if (vehicle == null)
                {
                    Chat.Output("No vehicle selected to buy!");
                    return;
                }

                Events.CallRemote("WatercraftPurchaseAttempt", hash, price, name, vehicle);
            });
            Events.Add("watercraftShop:RotatePlus", (object[] args) =>
            {
                initRota -= 40f;
                vehicle.SetHeading(initRota);
            });
            Events.Add("watercraftShop:RotatePlus", (object[] args) =>
            {
                initRota += 40f;
                vehicle.SetHeading(initRota);
            });
        }
    }
}
