using RAGE;
using System;

namespace Main.CEF {
    public class BankWindow : HtmlWindow {
        public BankWindow() {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "Bank";
            WindowPath = "package://ServerUI/index.html#/Bank";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady() {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents() {
            // Server -> Client -> CEF requests
            Events.Add("bank:Close", (object[] args) => {
                HideWindow();
                Chat.Activate(true);
            });
            Events.Add("bank:Open", (object[] args) => {
                RenderWindow();
                Events.CallRemote("BankOpen");
                Chat.Activate(false);
            });
            Events.Add("bank:setBalance", (object[] args) => {
                CefWindow.ExecuteJs($"EventBus.$emit('setBankBalance', '{(int)args[0]}', '{args[1]}');");
            });
            Events.Add("bank:deposit", (object[] args) => {
                Events.CallRemote("deposit", args[0]);
            });
            Events.Add("bank:withdraw", (object[] args) => {
                Events.CallRemote("withdraw", args[0]);
            });
            Events.Add("bank:Send", (object[] args) =>
            {
                Events.CallRemote("send", args[0], args[1]);
            });
        }
    }
}
