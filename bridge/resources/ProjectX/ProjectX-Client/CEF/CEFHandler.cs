using RAGE;

namespace Main.CEF {
    public class CEFHandler : Events.Script {
        public static BankWindow BankWindow { get; private set; }
        public static VehicleSpawner VehicleWindow { get; private set; }
        public static VehicleShopWindow VehicleShopWindow { get; private set; }
        public static WatercraftShopWindow WatercraftShopWindow { get; private set; }
        public static WatercraftSpawner WatercraftSpawner { get; private set; }
        public static AircraftShopWindow AircraftShopWindow { get; private set; }
        public static AircraftSpawner AircraftSpawner { get; private set; }
        public static TuningWindow TuningWindow { get; private set; }
        public static LoginWindow LoginWindow { get; private set; }
        public static ProfessionSelectionWindow ProfessionSelectionWindow { get; private set; }
        public static HUDWindow HUDWindow { get; private set; }
        public static WeaponShopWindow WeaponShopWindow { get; private set; }
        public static FactionCreateWindow FactionCreateWindow { get; private set; }
        public static HouseWindow HouseWindow { get; private set; }
        public static TruckerWindow TruckerWindow { get; private set; }
        public static FactionOverview FactionOverview { get; private set; }
        public static FactionSettingsWindow FactionSettingsWindow { get; private set; }
        public static ClothingShop ClothingShop { get; private set; }
        public CEFHandler() {
            RegisterWindows();
        }
        public static void RegisterWindows() {
            BankWindow = new BankWindow();
            VehicleWindow = new VehicleSpawner();
            VehicleShopWindow = new VehicleShopWindow();
            WatercraftShopWindow = new WatercraftShopWindow();
            WatercraftSpawner = new WatercraftSpawner();
            AircraftShopWindow = new AircraftShopWindow();
            AircraftSpawner = new AircraftSpawner();
            TuningWindow = new TuningWindow();
            LoginWindow = new LoginWindow();
            ProfessionSelectionWindow = new ProfessionSelectionWindow();
            HUDWindow = new HUDWindow();
            WeaponShopWindow = new WeaponShopWindow();
            FactionCreateWindow = new FactionCreateWindow();
            HouseWindow = new HouseWindow();
            TruckerWindow = new TruckerWindow();
            FactionOverview = new FactionOverview();
            FactionSettingsWindow = new FactionSettingsWindow();
            ClothingShop = new ClothingShop();
        }
    }
}
