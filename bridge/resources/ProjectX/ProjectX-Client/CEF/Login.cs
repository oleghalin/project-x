using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RAGE;
using RAGE.Elements;
using System;
using System.Collections.Generic;

namespace Main.CEF
{
    public class LoginWindow : HtmlWindow
    {

        public LoginWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            WindowPath = "package://ServerUI/index.html#/Login";
            Route = "Login";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("login:Close", (object[] args) => 
            {
                HideWindow();
                Chat.Activate(true);
            });
            Events.Add("login:Open", (object[] args) => 
            {
                RenderWindow();
                Chat.Activate(false);
            });
            Events.Add("loginInformationToServer", (object[] args) =>
            {
                Events.CallRemote("OnPlayerLoginAttempt", args[0], args[1]);
            });
            Events.Add("registerInformationToServer", (object[] args) =>
            {
                Events.CallRemote("OnPlayerRegisterAttempt", args[0], args[1]);
            });
        }
    }
}
