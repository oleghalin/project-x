﻿using RAGE;
using RAGE.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectX_Client.Features.Criminal
{
    public class CriminalHQ : Events.Script
    {
        List<Marker> HQMarkers = new List<Marker>();
        public CriminalHQ()
        {
            Events.Add("Criminal", (object[] args) =>
            {
                HQMarkers.Add(new Marker(1, new Vector3(1395.335f, 3613.044f, 34.98093f - 1f), 1, new Vector3(), new Vector3(), new RGBA(200, 200, 50)));
            });
            Events.Add("DeleteJobs", (object[] args) =>
            {
                if (HQMarkers.Count != 0)
                {
                    foreach (var marker in HQMarkers)
                    {
                        marker.Destroy();
                    }
                }
            });
        }
    }
}
