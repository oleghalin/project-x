﻿using RAGE;
using RAGE.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectX_Client.Features.Jobs
{
    internal class CarJacker : Events.Script
    {
        internal int areaBlip;
        internal int tick = 0;
        internal Marker marker;
        internal Blip blip;
        internal List<(Marker, TextLabel, Blip)> Job = new List<(Marker, TextLabel, Blip)>();
        internal CarJacker()
        {
            Events.Add("CarJacker:Spawn", Spawn);
            Events.Add("CarJacker:Despawn", Despawn);
            Events.Add("CarJacker:Start", Start);
            Events.Add("CarJack:FoundCar", FoundCar);
            Events.Add("CarJacker:Completed", Completed);
            Events.Add("CarJacker:Failed", Failed);
            Events.Add("CarJacker:Failed2", Failed2);
            areaBlip = RAGE.Game.Ui.AddBlipForRadius(0f, 0f, 0f, 200f);
            RAGE.Game.Ui.SetBlipColour(areaBlip, 17);
            RAGE.Game.Ui.SetBlipAlpha(areaBlip, 0);
        }

        internal void Spawn(object[] args)
        {
            var marker = new Marker(27, new Vector3(384.4128f, -1619.039f, 29.29196f), 2, new Vector3(), new Vector3(), new RGBA(255, 200, 60));
            var label = new TextLabel(new Vector3(384.4128f, -1619.039f, 29.29196f), "Press [E] to start job", new RGBA(255, 255, 255), 1, 0, true);
            var blip = new Blip(100, new Vector3(384.4128f, -1619.039f, 29.29196f), "Car Jacker Job", 0.5f, 5, 255, 0, true);
            Job.Add((marker, label, blip));
        }

        internal void Despawn(object[] args)
        {
            if (Job.Count != 0)
            {
                foreach((Marker marker, TextLabel label, Blip blip) in Job)
                {
                    marker.Destroy();
                    label.Destroy();
                    blip.Destroy();
                }
            }
        }

        internal void Start(object[] args)
        {
            var vector = (Vector3)args[0];
            RAGE.Game.Ui.SetBlipCoords(areaBlip, vector.X, vector.Y, vector.Z);
            RAGE.Game.Ui.SetBlipAlpha(areaBlip, 200);
            RAGE.Game.Ui.SetBlipSprite(areaBlip, 10);
            RAGE.Game.Ui.SetBlipRoute(areaBlip, true);
            Events.Tick += OnTick;
        }

        internal void FoundCar(object[] args)
        {
            var veh = (Vehicle)args[0];
            RAGE.Game.Ui.AddBlipForEntity(veh.Handle);
            Events.Tick -= OnTick;
            marker = new Marker(1, new Vector3(400.5809f, -1631.747f, 29.29192f -1), 2, new Vector3(), new Vector3(), new RGBA(255, 255, 255));
            blip = new Blip(1, marker.Position);
            blip.SetRoute(true);
            RAGE.Game.Ui.SetBlipAlpha(areaBlip, 0);
        }

        internal void Failed(object[] args)
        {
            RAGE.Game.Ui.SetBlipAlpha(areaBlip, 0);
            RAGE.Game.Ui.SetBlipRoute(areaBlip, false);
            Events.Tick -= OnTick;
        }

        internal void Failed2(object[] args)
        {
            RAGE.Game.Ui.SetBlipAlpha(areaBlip, 0);
            RAGE.Game.Ui.SetBlipRoute(areaBlip, false);
            Events.Tick -= OnTick;
            blip.SetRoute(false);
            blip.Destroy();
            marker.Destroy();
        }

        internal void Completed(object[] args)
        {
            blip.SetRoute(false);
            blip.Destroy();
            marker.Destroy();
        }

        internal void OnTick(List<Events.TickNametagData> nametags)
        {
            tick++;

            if (tick == 60)
            {
                tick = 0;
                Events.CallRemote("PlayerFindCar");
            }
        }
    }
}
