﻿using RAGE;
using RAGE.Elements;
using System.Collections.Generic;

namespace Main.Features.Jobs
{
    public class MedicMain : Events.Script
    {
        public Dictionary<Vector3, float> Hospitals = new Dictionary<Vector3, float>()
        {
            { new Vector3(-449.4493f, -341.0733f, 34.50176f), 78.64225f },
            { new Vector3(1839.369f, 3672.729f, 34.27672f), 214.9673f },
            { new Vector3(-247.6842f, 6331.134f, 32.42616f), 224.4187f },
            { new Vector3(298.4828f, -584.5302f, 43.26084f), 68.43027f }

        };

        public Blip Blip { get; set; }

        public MedicMain()
        {
            Events.Add("MedicFindPlayers", (object[] args) =>
            {
                Blip = new Blip(1, (Vector3)args[0], "Player", 0.8f, 0, 255, 50, true);
                Blip.SetRoute(true);
            });
            Events.Add("RemovePlayerBlip", (object[] args) =>
            {
                Blip.Destroy();
            });
            Events.Add("AddHospitalBlips", (object[] args) =>
            {
                foreach (KeyValuePair<Vector3, float> hospital in Hospitals)
                {
                    new Blip(80, hospital.Key, "Hospital", 0.5f, 41, 255, 50, true);
                }
            });
        }
    }
}
