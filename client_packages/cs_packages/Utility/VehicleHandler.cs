using RAGE;
using RAGE.Game;
using System;
using System.Collections.Generic;

namespace Main
{
    public class VehicleHandler : Events.Script
    {
        public readonly RAGE.Elements.Player ThePlayer = RAGE.Elements.Player.LocalPlayer;
        float speed = 0.0f;
        Vector3 rel_vector = new Vector3();
        float angle = 0.0f;
        float deadzone = 0.0f;

        float slope = 35.0f;
        float power_adj = 1.0f;
        float torque_adj = 1.0f;
        float angle_impact = 3.0f;
        float speed_impact = 2.0f;

        float speed_mult = 0.0f;
        float power_mult = 1.0f;
        float torque_mult = 1.0f;

        int accelval = 127;
        int brakeval = 127;

        int disablep = 0;
        int disablet = 0;
        int debug = 0;

        public VehicleHandler()
        {
            Events.Tick += OnTick;
        }

        private void OnTick(List<Events.TickNametagData> nametags)
        {
            if (ThePlayer == null) return;
            if (ThePlayer.Vehicle == null) return;
            var vehicle = ThePlayer.Vehicle;
            if (RAGE.Game.Vehicle.IsThisModelACar(vehicle.GetModel()))
            {
                if (slope < 0.0)
                    slope = 35.0f;
                speed = vehicle.GetSpeed();

                rel_vector = vehicle.GetSpeedVector(true);
                angle = (float)Math.Acos(rel_vector.Y / speed) * 180.0f / 3.14159265f;
                if (speed < slope)
                {
                    speed_mult = (slope - speed) / slope;
                }
                power_mult = 1.0f + power_adj * (((angle / 90) * angle_impact) + ((angle / 90) * speed_mult * speed_impact));
                torque_mult = 2.0f + torque_adj * (((angle / 90) * angle_impact) + ((angle / 90) * speed_mult * speed_impact));
                accelval = Pad.GetControlValue(0, 71);
                brakeval = Pad.GetControlValue(0, 72);
                // no need to worry about values since we compare them with each other
                if (!(angle > 90 && brakeval > accelval + 12) && angle > deadzone)
                {
                    if (disablet == 0)
                    {
                        vehicle.SetEngineTorqueMultiplier(torque_mult);
                    }
                    if (disablep == 0)
                    {
                        vehicle.SetEnginePowerMultiplier(power_mult);
                    }
                }
                else
                {
                    power_mult = 1.0f;
                    torque_mult = 1.0f;
                }
            }
        }
    }
}
