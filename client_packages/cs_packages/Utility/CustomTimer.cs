using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using RAGE;

namespace ProjectX_Client.Utility
{
    internal class CustomProgress : Events.Script
    {
        static int Counter { get; set; } = 0;
        static int StartingValue = 0;
        static Timer aTimer { get; set; } = null;
        static int ResX = 0;
        static int ResY = 0;
        static int Width = 0;
        static int MaxWidth = 200;
        static int MaxHeight = 25;
        static bool Active = false;
        static string CallEvent { get; set; } = null;

        internal CustomProgress()
        {
            Events.Add("Progress:Start", (object[] args) =>
            {
                if (!Active)
                {
                    Counter = (int)args[0];
                    StartingValue = Counter;
                    CallEvent = (string)args[1];
                    Events.Tick += ProgressTick;
                    Active = true;
                    aTimer = new Timer((obj) =>
                    {
                        TimerTick();
                    }, null, 0, 1000);
                }
                
            });
            Events.Add("Progress:Stop", (object[] args) =>
            {
                Events.Tick -= ProgressTick;
                Width = 200;
                if (aTimer != null)
                {
                    aTimer.Dispose();
                    aTimer = null;
                }
                if (CallEvent != null) CallEvent = null;
                Counter = 0;
                Width = 0;
                Active = false;
            });

            RAGE.Game.Graphics.GetScreenResolution(ref ResX, ref ResY);
        }

        internal static void ProgressTick(List<Events.TickNametagData> nametags)
        {
            RAGE.Game.UIRectangle.Draw(new Point(ResX - ResX / 2 - MaxWidth / 2, ResY - ResY / 5 - MaxHeight / 2), new Size(MaxWidth, MaxHeight), Color.Black);
            RAGE.Game.UIRectangle.Draw(new Point(ResX - ResX / 2 - MaxWidth / 2, ResY - ResY / 5 - MaxHeight / 2), new Size(Width, MaxHeight), Color.Red);
        }

        internal void TimerTick()
        {
            Counter--;
            Width = MaxWidth - (Counter*MaxWidth/StartingValue);
            if (Counter == 0)
            {
                Width = 200;
                aTimer.Dispose();
                aTimer = null;
                Counter = 0;
                Task.Delay(1000).ContinueWith((task => 
                {
                    Events.Tick -= ProgressTick;
                    Width = 0;
                    Events.CallRemote(CallEvent);
                    Active = false;
                    CallEvent = null;
                }));
            }
        }
    }
    internal class CustomTimer : Events.Script
    {
        static int Counter { get; set; } = 0;
        static Timer aTimer { get; set; } = null;
        static int ResX = 0;
        static int ResY = 0;
        static string Message { get; set; } = null;
        static string CallEvent { get; set; } = null;
          
        internal CustomTimer()
        {
            Events.Add("Timer:Start", (object[] args) =>
            {
                Counter = (int)args[0];
                CallEvent = (string)args[1];
                Message = (string)args[2];
                if (Counter == 0) Counter = 10;
                Events.Tick += Tick;
                aTimer = new Timer((obj) =>
                {
                    TimerTick();
                }, null, 0, 1000);
            });
            Events.Add("Timer:Stop", (object[] args) =>
            {
                aTimer.Dispose();
                Events.Tick -= Tick;
            });

            RAGE.Game.Graphics.GetScreenResolution(ref ResX, ref ResY);
        }

        internal static void Tick(List<Events.TickNametagData> nametags)
        {
            RAGE.Game.UIText.Draw($"{Message} [ {Convert.ToSingle(Counter)} ] seconds", new Point(ResX - ResX / 2, ResY - ResY / 2 - 250), 1f, Color.DarkRed, RAGE.Game.Font.ChaletComprimeCologne, true);
        }

        internal static void TimerTick()
        {
            Counter--;
            if (Counter == 0)
            {
                Events.Tick -= Tick;
                aTimer.Dispose();
                aTimer = null;
                Counter = 0;
                Events.CallRemote(CallEvent);
            }
        }

    }
}
