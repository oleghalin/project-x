using RAGE;
using RAGE.Elements;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectX_Client.Utility
{
   

    class PlayerUtility : Events.Script
    {
        public PlayerUtility()
        {
            Events.Add("DisableFiring", (object[] args) =>
            {
                var disable = (bool)args[0];
                if (disable)
                {
                    Events.Tick += Tick;
                }
                else if (!disable)
                {
                    Events.Tick -= Tick;
                }
            });
            Events.Add("GiveWeapon", (object[] args) =>
            {
                var weapon = (int)args[0];
                var ammo = (int)args[1];
                var weaponGroup = RAGE.Game.Weapon.GetWeapontypeGroup((uint)weapon);
                RemoveWeaponsInSlot(weaponGroup);
                Player.LocalPlayer.GiveWeaponTo((uint)weapon, ammo, false, true);
                Events.CallRemote("GiveWeapon", (uint)weapon, ammo);
            });
        }

        internal void RemoveWeaponsInSlot(uint weaponGroup)
        {
            switch (weaponGroup)
            {
                case (uint)WeaponSlots.AssaultRifle:
                    foreach(uint value in Enum.GetValues(typeof(AssaultRifles)))
                    {
                        if (Player.LocalPlayer.HasGotWeapon(value, false))
                        {
                            Events.CallRemote("RemoveWeapon", value);
                        }
                    }
                    break;
                case (uint)WeaponSlots.Handgun:
                    foreach (uint value in Enum.GetValues(typeof(Handguns)))
                    {
                        if (Player.LocalPlayer.HasGotWeapon(value, false))
                        {
                            Events.CallRemote("RemoveWeapon", value);
                        }
                    }
                    break;
                case (uint)WeaponSlots.HeavyWeapon:
                    foreach (uint value in Enum.GetValues(typeof(HeavyWeapons)))
                    {
                        if (Player.LocalPlayer.HasGotWeapon(value, false))
                        {
                            Events.CallRemote("RemoveWeapon", value);
                        }
                    }
                    break;
                case (uint)WeaponSlots.LightMachineGun:
                    foreach (uint value in Enum.GetValues(typeof(LightMachineGuns)))
                    {
                        if (Player.LocalPlayer.HasGotWeapon(value, false))
                        {
                            Events.CallRemote("RemoveWeapon", value);
                        }
                    }
                    break;
                case (uint)WeaponSlots.MachineGun:
                    foreach (uint value in Enum.GetValues(typeof(MachineGuns)))
                    {
                        if (Player.LocalPlayer.HasGotWeapon(value, false))
                        {
                            Events.CallRemote("RemoveWeapon", value);
                        }
                    }
                    break;
                case (uint)WeaponSlots.Melee:
                    foreach (uint value in Enum.GetValues(typeof(Melee)))
                    {
                        if (Player.LocalPlayer.HasGotWeapon(value, false))
                        {
                            Events.CallRemote("RemoveWeapon", value);
                        }
                    }
                    break;
                case (uint)WeaponSlots.Misc:

                    break;
                case (uint)WeaponSlots.Shotgun:
                    foreach (uint value in Enum.GetValues(typeof(Shotguns)))
                    {
                        if (Player.LocalPlayer.HasGotWeapon(value, false))
                        {
                            Events.CallRemote("RemoveWeapon", value);
                        }
                    }
                    break;
                case (uint)WeaponSlots.Sniper:
                    foreach (uint value in Enum.GetValues(typeof(SniperRifles)))
                    {
                        if (Player.LocalPlayer.HasGotWeapon(value, false))
                        {
                            Events.CallRemote("RemoveWeapon", value);
                        }
                    }
                    break;
                case (uint)WeaponSlots.Throwable:
                    break;
                case (uint)WeaponSlots.Unarmed:
                    break;
                default:
                    break;
            }
        }
        
        public void Tick(List<Events.TickNametagData> nametags)
        {
            RAGE.Game.Player.DisablePlayerFiring(true);
        }
    }
}
