
namespace ProjectX_Client.Utility
{

    enum WeaponSlots : uint
    {
        Unarmed = 2685387236,
        Melee = 3566412244,
        Handgun = 416676503,
        MachineGun = 3337201093,
        Shotgun = 860033945,
        AssaultRifle = 970310034,
        LightMachineGun = 1159398588,
        Sniper = 3082541095,
        HeavyWeapon = 2725924767,
        Throwable = 1548507267,
        Misc = 4257178988
    }

    enum Weapons : uint
    {
        SniperRifle = 100416529,
        FireExtinguisher = 101631238,
        CompactGrenadeLauncher = 125959754,
        Snowball = 126349499,
        VintagePistol = 137902532,
        CombatPDW = 171789620,
        HeavySniper = 205991906,
        SweeperShotgun = 317205821,
        MicroSMG = 324215364,
        Wrench = 419712736,
        Pistol = 453432689,
        PumpShotgun = 487013001,
        APPistol = 584646201,
        Ball = 600439132,
        Molotov = 615608432,
        SMG = 736523883,
        StickyBomb = 741814745,
        PetrolCan = 883325847,
        StunGun = 911657153,
        HeavyShotgun = 984333226,
        Minigun = 1119849093,
        GolfClub = 1141786504,
        FlareGun = 1198879012,
        Flare = 1233104067,
        GrenadeLauncherSmoke = 1305664598,
        Hammer = 1317494643,
        CombatPistol = 1593441988,
        Gusenberg = 1627465347,
        CompactRifle = 1649403952,
        HomingLauncher = 1672152130,
        Nightstick = 1737195953,
        Railgun = 1834241177,
        SawnOffShotgun = 2017895192,
        BullpupRifle = 2132975508,
        Firework = 2138347493,
        CombatMG = 2144741730,
        CarbineRifle = 2210333304,
        Crowbar = 2227010557,
        Flashlight = 2343591895,
        Dagger = 2460120199,
        Grenade = 2481070269,
        PoolCue = 2484171525,
        Bat = 2508868239,
        Pistol50 = 2578377531,
        Knife = 2578778090,
        MG = 2634544996,
        BullpupShotgun = 2640438543,
        BZGas = 2694266206,
        Unarmed = 2725352035,
        GrenadeLauncher = 2726580491,
        NightVision = 2803906140,
        Musket = 2828843422,
        ProximityMine = 2874559379,
        AdvancedRifle = 2937143193,
        RPG = 2982836145,
        PipeBomb = 3125143736,
        MiniSMG = 3173288789,
        SNSPistol = 3218215474,
        AssaultRifle = 3220176749,
        SpecialCarbine = 3231910285,
        Revolver = 3249783761,
        MarksmanRifle = 3342088282,
        BattleAxe = 3441901897,
        HeavyPistol = 3523564046,
        KnuckleDuster = 3638508604,
        MachinePistol = 3675956304,
        MarksmanPistol = 3696079510,
        Machete = 3713923289,
        SwitchBlade = 3756226112,
        AssaultShotgun = 3800352039,
        DoubleBarrelShotgun = 4019527611,
        AssaultSMG = 4024951519,
        Hatchet = 4191993645,
        Bottle = 4192643659,
        Parachute = 4222310262,
        SmokeGrenade = 4256991824
    }

    enum Misc : uint
    {
        Parachute = 4222310262,
        NightVision = 2803906140,
    }

    enum Throwable : uint
    {
        Grenade = 2481070269,
        StickyBomb = 741814745,
        ProximityMine = 2874559379,
        BZGas = 2694266206,
        Molotov = 615608432,
        FireExtinguisher = 101631238,
        PetrolCan = 883325847,
        Flare = 1233104067,
        Ball = 600439132,
        Snowball = 126349499,
        SmokeGrenade = 4256991824,
        PipeBomb = 3125143736
    }

    enum HeavyWeapons : uint
    {
        GrenadeLauncher = 2726580491,
        RPG = 2982836145,
        Minigun = 1119849093,
        Firework = 2138347493,
        Railgun = 1834241177,
        HomingLauncher = 1672152130,
        GrenadeLauncherSmoke = 1305664598,
        CompactGrenadeLauncher = 125959754
    }

    enum Shotguns : uint
    {
        PumpShotgun = 487013001,
        SawnOffShotgun = 2017895192,
        BullpupShotgun = 2640438543,
        AssaultShotgun = 3800352039,
        HeavyShotgun = 984333226,
        DoubleBarrelShotgun = 4019527611,
        SweeperShotgun = 317205821,
        AutoShotgun = 317205821
    }

    enum Melee : uint
    {
        Knife = 2578778090,
        Nightstick = 1737195953,
        Hammer = 1317494643,
        Bat = 2508868239,
        Crowbar = 2227010557,
        GolfClub = 1141786504,
        Bottle = 4192643659,
        Dagger = 2460120199,
        Hatchet = 4191993645,
        KnuckleDuster = 3638508604,
        Machete = 3713923289,
        Flashlight = 2343591895,
        SwitchBlade = 3756226112,
        PoolCue = 2484171525,
        Wrench = 419712736,
        BattleAxe = 3441901897
    }

    enum Handguns : uint
    {
        Pistol = 453432689,
        PistolMK2 = 3219281620,
        CombatPistol = 1593441988,
        Pistol50 = 2578377531,
        SNSPistol = 3218215474,
        HeavyPistol = 3523564046,
        VintagePistol = 137902532,
        MarksmanPistol = 3696079510,
        Revolver = 3249783761,
        APPistol = 584646201,
        StunGun = 911657153,
        FlareGun = 1198879012
    }

    enum MachineGuns : uint
    {
        MicroSMG = 324215364,
        MachinePistol = 3675956304,
        SMG = 736523883,
        SMGMK2 = 2024373456,
        AssaultSMG = 4024951519,
        CombatPDW = 171789620,
        MiniSMG = 3173288789,
        Gusenberg = 1627465347
    }

    enum LightMachineGuns : uint
    {
        MG = 2634544996,
        CombatMG = 2144741730,
        CombatMGMK2 = 3686625920,
    }

    enum SniperRifles : uint
    {
        SniperRifle = 100416529,
        HeavySniper = 205991906,
        HeavySniperMK2 = 177293209,
        MarksmanRifle = 3342088282
    }

    enum AssaultRifles : uint
    {
        AssaultRifle = 3220176749,
        AssaultRifleMK2 = 961495388,
        CarbineRifle = 2210333304,
        CarbineRifleMK2 = 4208062921,
        AdvancedRifle = 2937143193,
        SpecialCarbine = 3231910285,
        BullpupRifle = 2132975508,
        CompactRifle = 1649403952
    }
}
