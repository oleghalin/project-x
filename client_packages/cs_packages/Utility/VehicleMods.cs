using RAGE.Elements;
using System.Collections.Generic;

namespace Main
{
    public class VehicleMods
    {
        internal List<int> Mods(Vehicle vehicle)
        {
            List<int> mods = new List<int>();
            for(int i = 0; i < 48; i++)
            {
                mods.Add(vehicle.GetMod(i));
            }
            
            /*mods.Add(vehicle.GetMod(1));
            mods.Add(vehicle.GetMod(2));
            mods.Add(vehicle.GetMod(3));
            mods.Add(vehicle.GetMod(4));
            mods.Add(vehicle.GetMod(5));
            mods.Add(vehicle.GetMod(6));
            mods.Add(vehicle.GetMod(7));
            mods.Add(vehicle.GetMod(8));
            mods.Add(vehicle.GetMod(9));
            mods.Add(vehicle.GetMod(10));
            mods.Add(vehicle.GetMod(11));
            mods.Add(vehicle.GetMod(12));
            mods.Add(vehicle.GetMod(13));
            mods.Add(vehicle.GetMod(14));
            mods.Add(vehicle.GetMod(15));
            mods.Add(vehicle.GetMod(16));
            mods.Add(vehicle.GetMod(17));
            mods.Add(vehicle.GetMod(18));
            mods.Add(vehicle.GetMod(19));
            mods.Add(vehicle.GetMod(20));
            mods.Add(vehicle.GetMod(21));
            mods.Add(vehicle.GetMod(22));
            mods.Add(vehicle.GetMod(23));
            mods.Add(vehicle.GetMod(24));
            mods.Add(vehicle.GetMod(25));
            mods.Add(vehicle.GetMod(26));
            mods.Add(vehicle.GetMod(27));
            mods.Add(vehicle.GetMod(28));
            mods.Add(vehicle.GetMod(29));
            mods.Add(vehicle.GetMod(30));
            mods.Add(vehicle.GetMod(31));
            mods.Add(vehicle.GetMod(32));
            mods.Add(vehicle.GetMod(33));
            mods.Add(vehicle.GetMod(34));
            mods.Add(vehicle.GetMod(35));
            mods.Add(vehicle.GetMod(36));
            mods.Add(vehicle.GetMod(37));
            mods.Add(vehicle.GetMod(38));
            mods.Add(vehicle.GetMod(39));
            mods.Add(vehicle.GetMod(40));
            mods.Add(vehicle.GetMod(41));
            mods.Add(vehicle.GetMod(42));
            mods.Add(vehicle.GetMod(43));
            mods.Add(vehicle.GetMod(44));
            mods.Add(vehicle.GetMod(45));
            mods.Add(vehicle.GetMod(46));
            mods.Add(vehicle.GetMod(47));
            mods.Add(vehicle.GetMod(48));*/

            return mods;
        }
    }
}
