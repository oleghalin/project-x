using RAGE;
using System;

namespace Main.CEF
{
    public class WatercraftSpawner : HtmlWindow
    {
        public WatercraftSpawner()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "WatercraftSpawner";
            WindowPath = "package://ServerUI/index.html#/WatercraftSpawner";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("watercraft:Close", (object[] args) => {
                HideWindow();
            });
            Events.Add("watercraft:Open", (object[] args) => {
                RenderWindow();
                Events.CallRemote("WatercraftOpen");
            });
            Events.Add("watercraft:setPlayerVehicles", (object[] args) => {
                CefWindow.ExecuteJs($"EventBus.$emit('setPlayerWatercraft', '{args[0]}');");
            });
            Events.Add("watercraft:spawn", (object[] args) => {
                Events.CallRemote("watercraft:selected", args[0]);
            });
        }
    }
}
