using RAGE;
using System;

namespace Main.CEF
{
    public class AircraftSpawner : HtmlWindow
    {
        public AircraftSpawner()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "AircraftSpawner";
            WindowPath = "package://ServerUI/index.html#/AircraftSpawner";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("aircraft:Close", (object[] args) => {
                HideWindow();
            });
            Events.Add("aircraft:Open", (object[] args) => {
                RenderWindow();
                Events.CallRemote("AircraftOpen");
            });
            Events.Add("aircraft:setPlayerVehicles", (object[] args) => {
                CefWindow.ExecuteJs($"EventBus.$emit('setPlayerAircraft', '{args[0]}');");
            });
            Events.Add("aircraft:spawn", (object[] args) => {
                Events.CallRemote("aircraft:selected", args[0]);
            });
        }
    }
}
