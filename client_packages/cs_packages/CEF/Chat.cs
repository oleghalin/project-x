using System;
using System.Collections.Generic;
using System.Text;
using RAGE;

namespace ProjectX_Client.CEF
{
    internal class Chat : Events.Script
    {
        internal RAGE.Ui.HtmlWindow ChatWindow { get; private set; }
        internal string PlayerName { get; set; }
        internal Chat()
        {
            RAGE.Ui.DefaultWindow.ExecuteJs("document.getElementById('chat').remove();");
            ChatWindow = new RAGE.Ui.HtmlWindow("package://Chat/index.html");
            ChatWindow.MarkAsChat();
            RAGE.Chat.Colors = true;
            RAGE.Chat.SafeMode = true;
            Events.Add("PlayerName", GetPlayerName);
            Events.Add("globalMessage", GlobalMessage);
            Events.Add("localMessage", LocalMessage);
            Events.Add("factionMessage", FactionMessage);
            
        }

        internal void GetPlayerName(object[] args)
        {
            PlayerName = (string)args[0];
        }

        internal void GlobalMessage(object[] args)
        {
            Events.CallRemote("GlobalMessage", (string)args[0]);
        }

        internal void LocalMessage(object[] args)
        {
            Events.CallRemote("LocalMessage", (string)args[0]);
        }

        internal void FactionMessage(object[] args)
        {
            Events.CallRemote("FactionMessage", (string)args[0]);
        }
    }
}
