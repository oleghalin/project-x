using RAGE;
using System;

namespace Main.CEF
{
    public class FactionOverview : HtmlWindow
    {
        public FactionOverview()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "FactionOverview";
            WindowPath = "package://ServerUI/index.html#/Faction";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("myfaction:Close", (object[] args) => {
                HideWindow();
                Chat.Activate(true);
            });
            Events.Add("myfaction:Open", (object[] args) => {
                RenderWindow();
                Chat.Activate(false);
            });
            Events.Add("myfaction:setFactionData", (object[] args) => {
                CefWindow.ExecuteJs($"EventBus.$emit('setFactionData', '{(string)args[0]}', '{(int)args[1]}', '{(int)args[2]}', '{(int)args[3]}', '{(string)args[4]}', '{(int)args[5]}', '{(string)args[6]}');");
            });
            Events.Add("faction:leave", (object[] args) => {
                Events.CallRemote("LeaveFaction");
            });

        }
    }
}
