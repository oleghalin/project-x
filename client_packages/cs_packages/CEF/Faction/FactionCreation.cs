using RAGE;
using System;

namespace Main.CEF
{
    public class FactionCreateWindow : HtmlWindow
    {
        public FactionCreateWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "FactionCreate";
            WindowPath = "package://ServerUI/index.html#/FactionCreate";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("faction:Close", (object[] args) => {
                HideWindow();
            });
            Events.Add("faction:Open", (object[] args) => {
                RenderWindow();
            });
            Events.Add("faction:CreateFaction", (object[] args) => {
                Events.CallRemote("OnFactionCreate", args[0], args[1], args[2]);
            });
        }
    }
}
