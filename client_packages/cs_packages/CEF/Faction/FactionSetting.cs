using RAGE;
using System;

namespace Main.CEF
{
    public class FactionSettingsWindow : HtmlWindow
    {
        public FactionSettingsWindow()
        {
            CloseWhenOtherRenders = false;
            LockCursorWhenOpen = true;
            Route = "FactionSettings";
            WindowPath = "package://ServerUI/index.html#/FactionSettings";
            CreateNew();
            CefWindow.ExecuteJs("openUrl('Home')");
        }
        public override void OnWindowReady()
        {
            CefWindow.ExecuteJs($"openUrl('{Route}')");
        }
        public override void RegisterEvents()
        {
            // Server -> Client -> CEF requests
            Events.Add("factionSetting:Close", (object[] args) => {
                HideWindow();
                Chat.Activate(true);
            });
            Events.Add("factionSetting:Open", (object[] args) => {
                RenderWindow();
                Chat.Activate(false);
            });
            Events.Add("factionSetting:invite", (object[] args) =>
            {
                Events.CallRemote("FactionInvite", (string)args[0]);
            });
            Events.Add("factionSetting:kick", (object[] args) =>
            {
                Events.CallRemote("FactionKick", (string)args[0]);
            });

        }
    }
}
